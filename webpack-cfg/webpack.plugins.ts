import type IForkTsCheckerWebpackPlugin from 'fork-ts-checker-webpack-plugin'
import TerserPlugin from 'terser-webpack-plugin'

// eslint-disable-next-line @typescript-eslint/no-var-requires
const ForkTsCheckerWebpackPlugin: typeof IForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin')

export const plugins = [
    new ForkTsCheckerWebpackPlugin({
        logger: 'webpack-infrastructure',
    }),
]

export const optimization = {
    minimize: true,
    minimizer: [
        new TerserPlugin({
            minify: TerserPlugin.esbuildMinify,
            // test: /\.(t|j)s(\?.*)?$/i,
        }),
    ],
}
