import path from 'path'
import type { Configuration } from 'webpack'
import { optimization, plugins } from './webpack.plugins'
import { rules } from './webpack.rules'

rules.push({
    test: /\.css$/,
    use: [
        { loader: 'style-loader' },
        { loader: 'css-loader' },
        {
            loader: 'postcss-loader',
            options: {
                postcssOptions: {
                    plugins: [require('tailwindcss')],
                },
            },
        },
    ],
})

export const rendererConfig: Configuration = {
    module: {
        rules,
    },
    plugins,
    resolve: {
        extensions: ['.js', '.ts', '.jsx', '.tsx', '.css'],
        alias: {
            '@': path.join(__dirname, '../src'),
        },
    },
    optimization: optimization,
}
