import prettier from 'prettier'
import { shell } from 'electron'
import os from 'os'
import cp from 'child_process'
import fs from 'fs-extra'
import path from 'path'
import { result, result_err } from '../util/ipc'

export function read(src: string): PT.ipcRe<string> {
    try {
        if (!fs.existsSync(src)) {
            return result_err('文件不存在')
        }
        const stat = fs.statSync(src)
        if (!stat.isFile()) {
            return result_err('不是可读文件')
        }
        const txt = fs.readFileSync(src, 'utf-8')
        return result(txt)
    } catch (error) {
        return result_err()
    }
}

/**
 * - 成功返回一个对象(自己保证与声明类型相符)
 * - 失败返回null
 * @param src
 * @returns
 */
export function read_json<T = any>(src: string): PT.ipcRe<T | null> {
    const red = read(src)
    if (!red.success) {
        return result_err(red.msg)
    }
    try {
        const data: T = JSON.parse(red.data)
        return result(data)
    } catch (error) {
        return result_err(src + ', json解析错误')
    }
}
/** 写文件 */
export function write(src: string, txt: string) {
    try {
        fs.writeFileSync(src, txt, 'utf-8')
        return result()
    } catch (error) {
        return result_err()
    }
}
/** 写json文件, json会自动格式化 */
export function write_json<T extends Record<string, any>>(src: string, jsn: T) {
    try {
        const txt = JSON.stringify(jsn)
        const txt2 = prettier.format(txt, {
            parser: 'json',
            tabWidth: 4,
        })
        console.log('json write', src)
        return write(src, txt2)
    } catch (error) {
        console.log('json write fail', src)

        return result_err()
    }
}

/** 安全的创建文件夹, 出现多级缺失也可以创建, 已存在不会创建 */
export function mk_dir(src: string) {
    if (fs.existsSync(src)) {
        return result()
    }
    try {
        const save = []
        let it = src
        while (!fs.existsSync(it)) {
            save.push(it)
            it = path.join(it, '..')
        }
        for (let i = save.length - 1; i >= 0; i--) {
            fs.mkdirSync(save[i])
        }
        return result()
    } catch (error) {
        return result_err()
    }
}

export function is_dir(src: string) {
    if (!fs.existsSync(src)) {
        return false
    }
    if (!fs.statSync(src).isDirectory()) {
        return false
    }
    return true
}
/** 在资源管理器中显示 */
export function show_in_folder(src: string) {
    if (!fs.existsSync(src)) {
        return
    }
    shell.showItemInFolder(src)
}
/** 用vscode打开 */
export function vscode(src: string) {
    if (os.platform() === 'win32') {
        cp.exec(` code ${src} `)
    } else {
        cp.exec(` open -a "Visual Studio Code" "${src}" `)
    }
}
