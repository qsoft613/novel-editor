import * as text from '../text'
import * as Err from '../err'
import { useObservable } from 'rxjs-hooks'
import clsx from 'clsx'

export function Like(p: FormSelfType.GroupLike) {
    const err_txt = p.err?.txt ?? ''
    const has_label = p.label?.has ?? true
    if (has_label) {
        return (
            <div
                className="grid"
                style={{
                    gridTemplateColumns: `${hand_long(p.label?.width) ?? '80px'} ${
                        hand_long(p.input?.width) ?? '1fr'
                    } ${p.rt ? hand_long(p.rt.width) : ''}`,
                    gridTemplateRows: `${hand_long(p.input?.height) ?? '40px'} ${Boolean(err_txt) ? 'auto' : ''}`,
                }}
            >
                <div className="box-border flex h-full w-full items-center justify-end whitespace-normal pr-2">
                    {(p.label.required ?? false) && <span className="text-base text-red-400">*</span>}
                    <span
                        className={clsx(
                            'text-tnblack select-all text-base text-opacity-60',
                            p.label?.nowarap ?? true ? 'whitespace-nowrap' : '',
                        )}
                    >
                        {p.label?.txt}
                    </span>
                </div>
                <div className="flex h-full w-full items-center justify-start ">{p.input?.node}</div>
                {Boolean(p.rt) && <div className="flex items-center">{p.rt.node}</div>}
                {/* 第二行 */}
                <div></div>
                <div className="box-border whitespace-normal px-1 text-sm text-red-400">
                    <Err.Base text={err_txt} />
                </div>
            </div>
        )
    }
    return (
        <div>
            <div className="flex h-full w-full items-center justify-start ">{p.input?.node}</div>
            <div className="box-border whitespace-normal px-1 text-sm text-red-400">
                <Err.Base text={err_txt} />
            </div>
        </div>
    )
}

function hand_long(val?: number | string) {
    if (typeof val === 'string') {
        return val
    }
    if (typeof val === 'number') {
        return val + 'px'
    }
    return null
}

export function Text<Form extends Record<string, any>>(p: FormSelfType.GroupText<Form>) {
    const label_txt = p.label?.txt ?? p.ctrl.field_show(p.field)
    const a = useObservable(() => p.ctrl.observe_field$(p.field))
    return (
        <Like
            input={{
                ...p.input,
                node: <text.Ctrled {...p.inputerOption} ctrl={p.ctrl} field={p.field} />,
            }}
            label={{
                ...p.label,
                required: a?.required,
                txt: label_txt + ':',
            }}
            err={{
                txt: a?.err,
            }}
            rt={p.rt}
        />
    )
}

/** 唤醒 */
export function Wake<Form extends Record<string, any>>(p: FormSelfType.GroupWake<Form>) {
    const label_txt = p.label?.txt ?? p.ctrl.field_show(p.field)
    const a = useObservable(() => p.ctrl.observe_field$(p.field))
    return (
        <Like
            input={{
                ...p.input,
                node: <text.Wake {...p.wakeOption} placeholder={a?.value || label_txt} />,
            }}
            label={{
                ...p.label,
                txt: label_txt + ':',
            }}
            err={{
                txt: a?.err?.[0],
            }}
            rt={p.rt}
        />
    )
}
