import { Base } from './base'
import type { BaseP } from './base'

export function Primary(p: BaseP) {
    const p2 = {
        ...p,
    }
    if (p2.bg_color_preset === undefined) {
        p2.bg_color_preset = 'primary'
    }
    if (p2.txt_color_preset === undefined) {
        p2.txt_color_preset = 'primary'
    }
    return <Base {...p2}>{p.children ?? '好'}</Base>
}
