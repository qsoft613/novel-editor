import { Base } from './base'
import type { BaseP } from './base'

export function Cancel(p: BaseP) {
    const p2 = {
        ...p,
    }
    if (p2.bg_color_preset === undefined) {
        p2.bg_color_preset = 'gray'
    }
    if (p2.txt_color_preset === undefined) {
        p2.txt_color_preset = 'gray'
    }
    return <Base {...p2}>{p.children ?? '取消'}</Base>
}
