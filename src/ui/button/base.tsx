import { Style } from '@/style'
import { Svg } from '@/svg'
import { Util } from '@/util'
import clsx from 'clsx'
import React, { useEffect, useState } from 'react'
import { Observable } from 'rxjs'

export interface BaseP {
    txt_color_preset?: 'blue' | 'white' | 'black' | 'gray' | 'primary'
    /** txt_color > txt_color_preset */
    txt_color?: Color.item
    bg_color_preset?: 'white' | 'blue' | 'blue-ghost' | 'gray' | 'primary'
    /** bg_color > bg_color_preset */
    bg_color?: Color.item
    children?: React.ReactNode
    onClick?(): void
    /** 51 */
    height?: number
    /** 17 */
    font_size?: number
    loading?: boolean
    /**
     * - 吐出 true 变为 loading 态
     */
    loading$?: Observable<boolean>
    disabled?: boolean
    /**
     * - 吐出 true 变为 disabled 态
     */
    disabled$?: Observable<boolean>
    /** '100% */
    width?: number | string
}
export function Base(p: BaseP) {
    const clrs = hand_color(p)

    // --- loading
    const [loading, next_loading] = useState(p.loading ?? false)
    useEffect(() => {
        next_loading(p.loading ?? false)
    }, [p.loading])
    useEffect(() => {
        if (!p.loading$) {
            return
        }
        const ob = p.loading$.subscribe(next_loading)
        return () => ob.unsubscribe()
    }, [])

    // --- disabled
    const [disabled, next_disabled] = useState(p.disabled ?? false)
    useEffect(() => {
        next_disabled(p.disabled ?? false)
    }, [p.disabled])
    useEffect(() => {
        if (!p.disabled$) {
            return
        }
        const ob = p.disabled$.subscribe(next_disabled)
        return () => ob.unsubscribe()
    }, [])

    return (
        <div
            className={clsx('relative flex  w-full select-none items-center justify-center text-base  font-medium', {
                'qy-click-able ': !p.disabled,
                'qy-click-notable': p.disabled === true,
            })}
            style={{
                height: p.height ?? 40,
                fontSize: p.font_size ?? 16,
                color: clrs.txtclr.string(),
                backgroundColor: clrs.bgclr.string(),
                width: p.width ?? '100%',
            }}
            onClick={() => {
                if (loading || disabled) {
                    return
                }
                p.onClick?.()
            }}
        >
            {p.loading && (
                <>
                    <Svg.Graph.Loading.Base color_start={Util.colors.fromrgb(255, 255, 255)} />
                    <div className="w-2"></div>
                </>
            )}
            {p.children}
        </div>
    )
}

function hand_color(p: BaseP) {
    let txtclr = p.txt_color
    const cur_theme_item = Style.theme.cur_item()

    if (!txtclr) {
        switch (p.txt_color_preset) {
            case 'primary':
                txtclr = Util.colors.fromhex(cur_theme_item.front as any)
                break
            case 'blue':
                txtclr = Util.colors.fromrgb(42, 101, 253)
                break
            case 'white':
                txtclr = Util.colors.fromrgb(255, 255, 255)
                break
            case 'black':
                txtclr = Util.colors.fromrgb(3, 11, 32)
                break
            case 'gray':
                txtclr = Util.colors.fromrgb(3, 11, 32, 0.7)
                break
            default:
                txtclr = Util.colors.fromrgb(42, 101, 253)
        }
    }
    let bgclr = p.bg_color
    if (!bgclr) {
        switch (p.bg_color_preset) {
            case 'primary':
                bgclr = Util.colors.fromhex(cur_theme_item.l700 as any)
                break
            case 'gray':
                bgclr = Util.colors.fromrgb(3, 11, 32, 0.05)
                break
            case 'blue':
                bgclr = Util.colors.fromrgb(42, 101, 253)
                break
            case 'blue-ghost':
                bgclr = Util.colors.fromrgb(42, 101, 253, 0.1)
                break
            case 'white':
                bgclr = Util.colors.fromrgb(255, 255, 255)
                break
            default:
                bgclr = Util.colors.fromrgb(255, 255, 255)
                break
        }
    }
    return {
        txtclr,
        bgclr,
    }
}
