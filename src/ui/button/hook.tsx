import { useState } from 'react'

export function useButtonLoading(defau?: boolean) {
    const b = defau ?? false
    const [loading, next_loading] = useState(b)
    return {
        /** 当前loading值 */
        now: loading,
        /** 变为 true */
        t() {
            next_loading(true)
        },
        /** 变为 false */
        f() {
            next_loading(false)
        },
    }
}
