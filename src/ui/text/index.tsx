import { Svg } from '@/svg'
import { pipe } from '@/util/data/pipe'
import clsx from 'clsx'
import { useEffect, useState } from 'react'
import { useObservable } from 'rxjs-hooks'

export function Base(p: FormSelfType.TextBase) {
    const [type, next_type] = useState<'text' | 'password'>(p.type ?? 'text')
    const disabled = p.disabled ?? false

    useEffect(() => {
        // 延时处理值
        if (!p.timeFormat) {
            return
        }
        const t = setTimeout(() => {
            const v2 = p.timeFormat?.formatter(p.value || '')!
            p.onChange?.(v2, p.value || '')
        }, p.timeFormat.ms ?? 1000)
        return () => {
            clearTimeout(t)
        }
    }, [p.value])
    useEffect(() => {
        // 延时执行钩子
        if (!p.timeEffect) {
            return
        }
        const t = setTimeout(() => {
            p.timeEffect?.effect(p.value || '')!
        }, p.timeEffect.ms ?? 1000)
        return () => {
            clearTimeout(t)
        }
    }, [p.value])

    return (
        <div
            className={clsx(
                'flex w-full items-center justify-between   transition-all',
                {
                    'border border-solid': true,
                    'border-red-400': p.err === true,
                    'border-tnblack border-opacity-10': !p.err,
                    'focus-within:border-qy-400  ': true,
                },
                {
                    'bg-tnipt_disable_bg cursor-not-allowed ': disabled,
                },
            )}
            style={{
                height: p.height ?? 36,
            }}
        >
            {Boolean(p.preNode) ? p.preNode : <div className="w-4"></div>}
            <input
                disabled={disabled}
                autoComplete="off"
                type={type}
                className={clsx('h-4/5 w-0 flex-grow border-none text-base outline-none', {
                    'cursor-not-allowed': p.disabled ?? false,
                    'bg-transparent': true,
                })}
                placeholder={p.placeholder}
                style={{
                    height: 'calc(100% - 6px)',
                }}
                value={p.value || ''}
                onChange={(e) => {
                    if (disabled) {
                        return
                    }
                    if (!p.onChange) {
                        return
                    }
                    const nval = pipe(
                        e.target.value,
                        (val) => {
                            if (p.rule?.len !== undefined) {
                                return val.slice(0, p.rule.len)
                            }
                            return val
                        },
                        (val) => {
                            const al = p.rule?.allow
                            if (al !== undefined) {
                                return val
                                    .split('')
                                    .filter((lt) => al.test(lt))
                                    .join('')
                            }
                            return val
                        },
                        (val) => {
                            if (p.rule?.notAllow !== undefined) {
                                return val.replace(p.rule.notAllow, '')
                            }
                            return val
                        },
                    )
                    p.onChange(nval, p.value || '')
                }}
            />
            <div className="w-6"></div>
            {p.type === 'password' && p.value?.length && (
                <div
                    className=" tn-click-able2 flex h-full items-center justify-center  "
                    style={{
                        minWidth: 20,
                        aspectRatio: '1 / 2',
                    }}
                    onClick={() => {
                        next_type(type === 'password' ? 'text' : 'password')
                    }}
                >
                    {type === 'password' ? <Svg.Graph.Eye.Close len={24} /> : <Svg.Graph.Eye.Open len={24} />}
                </div>
            )}
            {Boolean(p.suffNode) ? p.suffNode : <div className="hidden"></div>}
            {Boolean((p.clear?.has ?? true) && p.value?.length && !disabled) && (
                <div
                    className="qy-click-able2  flex h-full   items-center justify-center"
                    style={{
                        minWidth: 20,
                        aspectRatio: '1 / 2',
                    }}
                    onClick={() => {
                        p.onChange?.('', p.value || '')
                    }}
                >
                    <Svg.Graph.Clear.Base color_preset="gray" opacity={0.6} len={18} />
                </div>
            )}
            <div
                className="tn-click-able  flex h-full   items-center justify-center"
                style={{
                    aspectRatio: '1 / 4',
                }}
            ></div>
        </div>
    )
}

export function Ctrled<Form extends Record<string, any>>(p: FormSelfType.TextCtrled<Form>) {
    const ab = useObservable(() => p.ctrl.observe_field$(p.field))

    const val = (ab?.value || '') as string
    const err = Boolean(ab?.err?.at(0))
    const placeholder = p.placeholder ?? p.ctrl.field_show(p.field)
    const disabled = ab?.disabled ?? false
    return (
        <Base
            {...p}
            err={err}
            value={val}
            placeholder={placeholder}
            disabled={disabled}
            onChange={(s) => {
                p.ctrl.merge((fv: any) => {
                    fv[p.field] = s
                })
            }}
        />
    )
}

/** 点击唤醒动作... */
export function Wake(p: FormSelfType.Wake) {
    return (
        <div
            className={clsx(
                'flex w-full items-center justify-start rounded-full transition-all',
                'border-tnblack border border-solid border-opacity-10',
                'cursor-pointer hover:border-opacity-50',
                'group',
            )}
            style={{
                height: p.height ?? 48,
            }}
            onClick={p.onClick}
        >
            {Boolean(p.preNode) ? p.preNode : <div className="w-5"></div>}

            <span
                className={clsx(
                    'text-tnblack text-base text-opacity-30 ',
                    'transition-opacity group-hover:text-opacity-60',
                    'mr-auto',
                )}
            >
                {p.placeholder}
            </span>
            {p.suffNode ? (
                p.suffNode
            ) : (
                <div
                    className={clsx(
                        'ml-auto flex h-full  items-center justify-center',
                        'opacity-30 transition-all group-hover:opacity-60',
                    )}
                    style={{
                        minWidth: 20,
                        aspectRatio: '1 / 2',
                    }}
                >
                    <Svg.Graph.Direction.Base arrowTo="right" len={10} />
                </div>
            )}
            <div
                className="tn-click-able  flex h-full   items-center justify-center"
                style={{
                    aspectRatio: '1 / 4',
                }}
            ></div>
        </div>
    )
}
