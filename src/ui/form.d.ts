import { FormCtrlType } from '@tenn-web/no-ui/src/form/type'

declare global {
    declare namespace FormSelfType {
        /** 组 */
        interface GroupLike {
            label?: {
                /** true */
                has?: boolean
                txt?: string
                required?: boolean
                /** 80px */
                width?: number | string
                /** true */
                nowarap?: boolean
            }
            input?: {
                node?: React.ReactNode
                /** 400px */
                width?: number | string
                /** 48px */
                height?: number | string
            }
            /** 右上角 */
            rt?: {
                node?: React.ReactNode
                /** 400px */
                width: number | string
            }
            err?: {
                txt?: string
            }
        }
        /** 组文本(受控) */
        interface GroupText<Form extends Record<string, any>> extends GroupLike {
            ctrl: NOUI.form<Form>
            field: string
            inputerOption?: TextBase
        }
        /** 组唤醒 */
        interface GroupWake<Form extends Record<string, any>> extends GroupLike {
            ctrl: NOUI.form<Form>
            field: string
            wakeOption?: Wake
        }
        /** 文本 */
        interface TextBase {
            /** false */
            err?: boolean
            /** 默认 48 */
            height?: number
            value?: string
            onChange?: (next: string, now: string) => void
            disabled?: boolean
            preNode?: React.ReactNode
            suffNode?: React.ReactNode
            placeholder?: string
            type?: 'text' | 'password'
            timeFormat?: {
                /** 无输入 1000 ms 后格式化 */
                ms?: number
                formatter(val: string): string
            }
            timeEffect?: {
                /** 无输入 1000 ms 后格式化 */
                ms?: number
                effect(val: string): void
            }

            clear?: {
                /** true */
                has?: boolean
            }
            rule?: {
                len?: number
                /** 逐个字符过滤 */
                allow?: RegExp
                /** 整体过滤 */
                notAllow?: RegExp
            }
        }
        interface SuggestBase {
            /** 修改此值时, 组件内的输入文本会跟随变化
             * - undefined 或 '' 会清空
             */
            str_up?: string
            /** uuid */
            id?: string
            /** false */
            err?: boolean
            /** 默认 48 */
            height?: number
            /** FormUi.select_node['code'] */
            value?: FormUi.select_node['code']
            onChange?: (next: FormUi.select_node['code'] | undefined | null, node: FormUi.select_node | null) => void
            disabled?: boolean
            preNode?: React.ReactNode
            suffNode?: React.ReactNode
            placeholder?: string
            nodes?: FormUi.select_node[]

            clear?: {
                /** true */
                has?: boolean
            }
        }
        /** 文本(受控) */
        interface TextCtrled<Form extends Record<string, any>> extends TextBase {
            ctrl: NOUI.form<Form>
            field: string
        }
        /** 点击唤醒 */
        interface Wake {
            /** 默认 48 */
            height?: number
            disabled?: boolean
            preNode?: React.ReactNode
            suffNode?: React.ReactNode
            placeholder?: string
            onClick?(): void
        }
    }
}
