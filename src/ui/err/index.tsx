import { Util } from '@/util'
import { useObservable } from 'rxjs-hooks'

interface BaseP {
    text?: string | null
}
export function Base(p: BaseP) {
    if (!p.text) {
        return null
    }
    return (
        <div className="">
            <span
                className="select-all text-base"
                style={{
                    color: Util.colors.items.pick.string(),
                }}
            >
                {p.text}
            </span>
        </div>
    )
}

interface CtrledP {
    ctrl: NOUI.form<any>
    field: string
}
export function Ctrled(p: CtrledP) {
    const ab = useObservable(() => p.ctrl.observe_field$(p.field))
    return <Base text={ab?.err?.[0] || ''} />
}
