import { app, BrowserWindow, Menu, MenuItem, protocol, ipcMain } from 'electron'

declare const MAIN_WINDOW_WEBPACK_ENTRY: string

export function set_menu() {
    const menu = new Menu()

    menu.append(
        new MenuItem({
            label: '主页',
            click(menuItem, browserWindow, event) {
                browserWindow.loadURL(MAIN_WINDOW_WEBPACK_ENTRY)
            },
        }),
    )
    menu.append(
        new MenuItem({
            label: '编辑',
            click(menuItem, browserWindow, event) {
                browserWindow.webContents.send('publish', [1, '--'])
            },
        }),
    )

    Menu.setApplicationMenu(menu)
}
