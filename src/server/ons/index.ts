import { on_app } from './app'
import { on_book } from './book'
import { on_elec } from './elec'
import { fss } from './fss'

export function ipc_ons() {
    fss()
    on_app()
    on_book()
    on_elec()
}
