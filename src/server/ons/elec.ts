import { Util } from '@/util'
import { app, ipcMain, dialog } from 'electron'
import joi from 'joi'
import path from 'path'
import fs from 'fs'
import { Utiln } from '@/utiln'

const keys = Util.ipc.keys

export function on_elec() {
    ipcMain.on(keys.pick_dir, (e) => {
        dialog
            .showOpenDialog({
                properties: ['openDirectory'],
                filters: [],
            })
            .then((res) => {
                const src = res.filePaths.at(0) || ''
                return Util.ipc.reply(e, keys.pick_dir, Util.ipc.result(src))
            })
    })
    ipcMain.on(keys.pick_file, (e, extensions?: string[]) => {
        const exts = (extensions ?? []).filter((s) => typeof s === 'string')

        dialog
            .showOpenDialog({
                properties: ['openFile'],
                filters: [{ extensions: exts, name: '' }],
            })
            .then((res) => {
                const src = res.filePaths.at(0) || ''
                return Util.ipc.reply(e, keys.pick_file, Util.ipc.result(src))
            })
    })
}
