import { Util } from '@/util'
import { ipcMain } from 'electron'
import fs from 'fs'
import path from 'path'

export function fss() {
    ipcMain.on(Util.ipc.keys.read_file, (e, fspath) => {
        try {
            const ret = fs.readFileSync(fspath, 'utf-8')
            const res = Util.ipc.result(ret)
            Util.ipc.reply(e, Util.ipc.keys.read_file, res)
        } catch (error) {}
        Util.ipc.reply(e, Util.ipc.keys.read_file, Util.ipc.result_err())
    })
}
