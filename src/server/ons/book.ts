import { Util } from '@/util'
import { ipcMain } from 'electron'
import fs from 'fs'
import { mk } from '@/arena/book/item'
import joi from 'joi'
import dayjs from 'dayjs'
import path from 'path'
import { Utiln } from '@/utiln'
import { update } from './app'

const keys = Util.ipc.keys

export function on_book() {
    ipcMain.on(keys.load_books, (e, fspaths: string[]) => {
        try {
            const books = fspaths.map(load_book).filter((v) => v.src)

            books.sort((a, b) => a._sort - b._sort)
            const dto = Util.ipc.result(books)
            Util.ipc.reply(e, Util.ipc.keys.load_books, dto)
        } catch (error) {
            Util.ipc.reply(e, Util.ipc.keys.load_books, Util.ipc.result_err())
        }
    })
    ipcMain.on(keys.load_book, (e, fspath: string) => {
        try {
            const book = load_book(fspath)
            const dto = Util.ipc.result(book)
            dto.success = book._well
            Util.ipc.reply(e, Util.ipc.keys.load_book, dto)
        } catch (error) {
            Util.ipc.reply(e, Util.ipc.keys.load_book, Util.ipc.result_err())
        }
    })
    ipcMain.on(keys.save_book, (e, book: Book.item) => {
        try {
            const src_dir = path.resolve(book.src)
            const src_opt = path.join(src_dir, 'option.json')
            const book_final = Util.data.pipe(book, move_img)
            const re = Utiln.fs.write_json(src_opt, book_final)
            update((ap) => {
                if (ap.shelf_list.includes(src_dir)) {
                    return
                }
                ap.shelf_list.unshift(src_dir)
                return ap
            })

            Util.ipc.reply(e, Util.ipc.keys.save_book, re)
        } catch (error) {
            Util.ipc.reply(e, Util.ipc.keys.save_book, Util.ipc.result_err())
        }
    })
}

/** 保存书时, 移动封面图片 */
function move_img(book: Book.item) {
    if (!book.cover) {
        return book
    }
    if (!fs.existsSync(book.cover)) {
        return book
    }
    const old = path.resolve(book.cover)
    const ext = path.extname(old)
    const tar = path.join(book.src, `cover${ext}`)
    if (tar === old) {
        return book
    }
    fs.copyFileSync(old, tar)
    book.cover = tar
    return book
}

function load_book(fspath: string) {
    //
    console.log('load book')
    const src_opt = path.join(fspath, 'option.json')
    if (!fs.existsSync(src_opt)) {
        const err404 = mk()
        err404._well = false
        return err404
    }

    const readed = Utiln.fs.read_json<Book.item>(src_opt)
    const cooked = joi_v3(readed.data)

    cooked._well = true
    cooked.src = path.join(fspath)
    // 写入标准化的
    Utiln.fs.write_json(src_opt, cooked)

    if (!Utiln.fs.is_dir(fspath)) {
        cooked._well = false
    }

    if (!cooked._well) {
        cooked._sort = 1
    }
    console.log('load book end', fspath)

    return cooked
}

function joi_v3(raw: any) {
    raw = Object.assign({}, raw)
    const defau = mk()
    const schema = joi
        .object<Book.item_v3>({
            v: Util.joi.pub_schemas.version(),
            action: joi.required().only().allow('add', 'option', 'write').failover('write'),
            id: Util.joi.pub_schemas.id(),
            name: joi.string().required().failover(''),
            src: joi.string().required().failover(''),
            cover: joi.string().required().failover(''),
            edit_last_chapter: joi.string().required().failover(''),
            edit_last_time: Util.joi.pub_schemas.date_time(),
            edit_last_20_chapter: joi.array().items(joi.string()).required().failover([]),
            editer_size_max: joi.boolean().required().failover(false),
            work_blocks: joi.array().required().failover([]),
            // editer_size_x: joi.number().integer().required().failover(20),
            // editer_size_y: joi.number().integer().required().failover(20),
            // editer_size_w: joi.number().integer().required().failover(400),
            // editer_size_h: joi.number().integer().required().failover(600),
        })
        .failover(defau)
    const re = schema.validate(raw, Util.joi.schema_defau_opt()).value
    re._sort = Util.data.pipe(
        re.edit_last_time,
        dayjs,
        (v) => v.format('YYYYMMDDHHmm'),
        Number,
        (n) => n || 0,
    )

    return re as Book.item_v3
}
