import { mk } from '@/arena/app/item'
import { Util } from '@/util'
import { app, ipcMain } from 'electron'
import joi from 'joi'
import path from 'path'
import fs from 'fs'
import { Utiln } from '@/utiln'

const keys = Util.ipc.keys

export function on_app() {
    ipcMain.on(keys.load_app_option, (e) => {
        const opt = load()
        write(opt)
        Util.ipc.reply(e, keys.load_app_option, Util.ipc.result(opt))
    })
}

export function update(upper: (old: App.item) => App.item | void) {
    const old = load()
    const next = upper(old)
    if (next) {
        write(next)
    }
}

function load() {
    const defau = mk()
    const src_otp = path.join(app.getPath('documents'), 'qy-writer', 'option.json')
    try {
        Utiln.fs.mk_dir(path.join(src_otp, '..'))

        const jsn = Utiln.fs.read_json<App.item>(src_otp)
        if (!jsn.success) {
            throw ''
        }

        const re = joi_v3(jsn.data)
        // Utiln.fs.write_json(src_otp, re)
        return re

        // Util.ipc.reply(e, keys.load_app_option, Util.ipc.result(re))
    } catch (error) {
        // Utiln.fs.write_json(src_otp, defau)
        return defau
        // Util.ipc.reply(e, Util.ipc.keys.load_app_option, Util.ipc.result(defau))
    }
}
function write(opt: App.item) {
    const src_otp = path.join(app.getPath('documents'), 'qy-writer', 'option.json')
    try {
        Utiln.fs.write_json(src_otp, opt)
    } catch (error) {}
}

function joi_v3(raw: any) {
    raw = Object.assign({}, raw)

    const defau = mk()
    const schema = joi
        .object<App.item_v3>({
            shelf_list: joi.array().items(joi.string()).required().failover([]),
            ui_theme: joi.required().only().allow('word', 'excel', 'ppt').failover('word'),
            v: Util.joi.pub_schemas.version(),
        })
        .failover(defau)
    const re = schema.validate(raw, Util.joi.schema_defau_opt()).value
    // re.shelf_list = re.shelf_list.filter((v) => {
    //     if (!fs.existsSync(v)) {
    //         return false
    //     }
    //     if (!fs.statSync(v).isDirectory()) {
    //         return false
    //     }
    //     return true
    // })
    return re as App.item_v3
}
