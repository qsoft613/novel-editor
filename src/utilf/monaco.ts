import { cur_item } from '@/style/theme'
import * as monaco from 'monaco-editor'

export function make(dom: HTMLDivElement, opt?: monaco.editor.IStandaloneEditorConstructionOptions) {
    preset_theme()
    opt = opt ?? default_option()
    const editer = monaco.editor.create(dom, opt)
    return editer
}

export function default_option(): monaco.editor.IStandaloneEditorConstructionOptions {
    // const app_opt = SubAppOption.edit$.value
    const opt: monaco.editor.IStandaloneEditorConstructionOptions = {
        value: '',
        language: 'book',
        lineNumbers: 'off',
        theme: 'qy',
        wordWrap: 'on',
        fontSize: 16,
        lineHeight: 26,
        fontFamily: 'siyuangooglelight',
        // 分割文本
        wordSeparators: '~!@#$%^&*()-=+[{]}\\|;:\'",.<>/?，。？！……：”“—【】',
        minimap: {
            enabled: false,
        },
        // contextmenu: false,
        tabSize: 4,
        suggest: {
            /** 不提示本章同开头片段 */
            showWords: false,
        },
    }
    return opt
}

function preset_theme() {
    const curt = cur_item()

    monaco.editor.defineTheme('qy', {
        base: 'vs',
        inherit: true,
        rules: [],
        colors: {
            'editor.background': curt.l100,
            'editorLineNumber.foreground': curt.front,
            // 'editorMinimap.background': tm.color.themeLighter,
            // // 光标所在行
            'editor.lineHighlightBorder': curt.l200,
            'editor.lineHighlightBackground': curt.l200,
            'editor.rangeHighlightBackground': curt.l200,
            'editor.selectionHighlightBackground': curt.l200,
            // // 选中
            'editor.selectionBackground': curt.l300,
            // 'editorSuggestWidget.background': tm.color.themeLight,
            // 'editorSuggestWidget.selectedBackground': tm.color.themeLight,
            // 'editorSuggestWidget.highlightForeground': tm.color.themeLight,
            // 'list.hoverBackground': tm.color.themeLight,
            // 'editorSuggestWidget.foreground': tm.color.themePrimary, // 提示字颜色
            // // 光标颜色
            // 'editorCursor.foreground': curt.front,
        },
    })
}
