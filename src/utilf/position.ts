export function l(x: number) {
    x = x | 0
    if (x < 0) {
        return window.innerWidth + x
    }
    return x
}
export function r(x: number) {
    x = x | 0
    if (x < 0) {
        return window.innerWidth + x
    }
    return x
}
export function t(y: number) {
    y = y | 0
    if (y < 0) {
        return window.innerHeight + y
    }
    return y
}
export function b(y: number) {
    y = y | 0
    if (y < 0) {
        return window.innerHeight + y
    }
    return y
}
