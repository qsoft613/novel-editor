/**
 * 获取关于对象的key的提示
 * 返回一个对象, 其k,v为传入对象的key
 * ```
 * const obj = {
 *   age: 23
 *   name: 'qq'
 * }
 * const ks = key_key(obj)
 * {
 *   age: 'age',
 *   name: 'name',
 * }
 * ks. // 获取 age, name 提示, 且保留tsdoc注释
 * ```
 *
 */
export function key_key<T extends Record<string, any>>(obj: () => T): keykeyOfObj<T>
export function key_key<T extends Record<string, any>>(obj: T): keykeyOfObj<T>
export function key_key<T extends Record<string, any>>(obj: T | (() => T)) {
    if (typeof obj === 'function') {
        obj = obj()
    }
    const o: any = {}
    const ks = Object.keys(obj as Record<string, any>)
    ks.forEach((k) => {
        o[k] = k
    })
    return o as keykeyOfObj<T>
}

type keykeyOfObj<T extends Record<string, any>> = {
    [k in keyof T]-?: k
}
