/** 转为列表
 * - 迭代法, 从上到下, 从左到右
 */
export function toLi1<T extends PT.treeLike<ID>, ID extends PT.treeID>(tree: T) {
    let li: T[] = []
    let queue: T[] = [tree]
    while (queue.length) {
        li = li.concat(queue)
        let next: T[] = []
        queue.forEach((it) => {
            next = next.concat(it.children as T[])
        })
        queue = next
    }
    return li
}
/** 转为Map */
export function toMap<T extends PT.treeLike<ID>, ID extends PT.treeID>(tree: T) {
    const re: Map<T['id'], T> = new Map()
    let queue: T[] = [tree]
    while (queue.length) {
        let next: T[] = []
        queue.forEach((it) => {
            re.set(it.id, it)
            next = next.concat(it.children as T[])
        })
        queue = next
    }
    return re
}

/** 在树中查找, 返回一个列表 */
export function find<T extends PT.treeLike<ID>, ID extends PT.treeID>(tree: T, finder: (it: T) => boolean) {
    const re: T[] = []
    let queue: T[] = [tree]
    while (queue.length) {
        let next: T[] = []
        queue.forEach((it) => {
            if (finder(it)) {
                re.push(it)
            }
            next = next.concat(it.children as T[])
        })
        queue = next
    }
    return re
}
/** 在树中查找, 返回第一个节点 */
export function find1<T extends PT.treeLike<ID>, ID extends PT.treeID>(tree: T, finder: (it: T) => boolean) {
    let queue: T[] = [tree]
    while (queue.length) {
        let next: T[] = []
        for (const it of queue) {
            if (finder(it)) {
                return it
            }
            next = next.concat(it.children as T[])
        }
        queue = next
    }
    return null
}

/** 帮助设置 pid */
export function helpPid<T extends PT.treeLike>(tree: T, pNode?: T) {
    tree.pid = pNode?.id ?? null
    tree.children.forEach((cd) => {
        helpPid(cd, tree)
    })
}

interface fromP<T extends PT.treeFull<ID>, ID extends PT.treeID> {
    /** 原始节点 */
    raw: any
    /** - level 优先使用父节点+1
     * - children 不生效
     * - pid 不生效
     */
    cook: (it: any, parent?: T) => Omit<T, 'pid' | 'children' | 'level'>
    /** 如何获取原始节点的子节点列表 */
    children: (it: any) => any[]
    parent?: T
    /**
     * 默认 1开始
     */
    level?: number
}
/**
 * 尝试解析为树
 */
export function from<T extends PT.treeFull<ID>, ID extends PT.treeID>(p: fromP<T, ID>): null | T {
    const { raw, cook, children, parent } = p
    const level = p.level ?? 1
    if (typeof raw !== 'object') {
        return null
    }
    const cooked = cook(raw, parent) as T
    cooked.pid = parent?.id ?? null

    const _cds = children(raw)
    if (Array.isArray(_cds)) {
        cooked.children = _cds
            .map((it) =>
                from<T, ID>({
                    ...p,
                    parent: cooked,
                    raw: it,
                    level: level + 1,
                }),
            )
            .filter(Boolean) as T[]
    } else {
        cooked.children = []
    }
    return cooked
}

interface fromFullP<T extends PT.treeFull<ID>, ID extends PT.treeID> {
    /** 原始节点 */
    raw: any
    /** - level 优先使用父节点+1
     * - children 不生效
     * - pid 不生效
     */
    cook: (it: any, parent?: T) => Omit<T, 'pid' | 'children' | 'level'>
    /** 如何获取原始节点的子节点列表 */
    children: (it: any) => any[]
    parent?: T
    /**
     * 默认 1开始
     */
    level?: number
    map?: Map<ID, T>
    li?: T[]
}
/** 解析为树, 并获取map和array */
export function from_full<T extends PT.treeFull<ID>, ID extends PT.treeID = number>(
    p: fromFullP<T, ID>,
): {
    map: Map<ID, T>
    li: T[]
    tree: null | T
} {
    const { raw, cook, children, parent } = p
    const level = p.level ?? 1
    const map: Map<ID, T> = p.map ?? new Map()
    const li = p.li ?? []
    if (typeof raw !== 'object') {
        return {
            map,
            li,
            tree: null,
        }
    }
    const tree = deep(raw, undefined, level)

    function deep(raw: any, parent?: T, level?: number) {
        const cooked = cook(raw, parent) as T
        cooked.level = parent ? parent.level + 1 : level ?? 1
        cooked.pid = parent?.id ?? null
        map.set(cooked.id, cooked)
        li.push(cooked)
        const _cds = children(raw)
        cooked.children = _cds.map((cd) => {
            return deep(cd, cooked)
        })
        return cooked
    }
    return {
        map,
        li,
        tree,
    }
}
