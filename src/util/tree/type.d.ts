declare namespace PT {
    /** 基本要素的树 */
    interface treeLike<ID extends treeID = string> {
        id: ID
        pid: ID | null
        children: treeLike<ID>[]
    }
    /** 完全能力的树 */
    interface treeFull<ID extends treeID = string> extends treeLike<ID> {
        level: number
        children: treeFull<ID>[]
    }
    type treeID = string | number
}
