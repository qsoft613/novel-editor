declare namespace Color {
    /** 根据 color 对象收窄类型 */
    interface item {
        /** rgb | rgba */
        array(): number[]
        /** 修改 alpha */
        alpha(a: number): item
        /** 修改 red */
        red(a: number): item
        /** 修改 blue */
        blue(a: number): item
        /** 修改 green */
        green(a: number): item
        /** rgba?(,,,) */
        string(): string
    }
}
