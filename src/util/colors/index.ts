import { Style } from '@/style'
import COLOR from 'color'

/** 预置颜色对象 */
export const items = {
    /** 42,101,253 */
    blue: fromrgb(42, 101, 253),
    black: fromrgb(3, 11, 42),
    /** 255, 122, 122 */
    pick: fromrgb(255, 122, 122),
} as const

/** 从 rgb 转为 color 对象 */
export function fromrgb(r: number, g: number, b: number, a?: number): Color.item {
    return COLOR([r, g, b, a], 'rgb') as unknown as Color.item
}

/** 从 hex 转为 color 对象 */
export function fromhex(hex: `#${string}`): Color.item {
    return COLOR(hex, 'hex') as unknown as Color.item
}

/** 获取 x 个颜色的中间色
 * - 单纯的rgb均值
 */
export function mid(...clrs: Color.item[]) {
    let sums = [0, 0, 0, 0]
    clrs.forEach((clr) => {
        const arr = clr.array()
        sums[0] += arr[0]
        sums[1] += arr[1]
        sums[2] += arr[2]
        sums[3] += arr[3] || 1
    })
    sums[0] = (sums[0] / clrs.length) | 0
    sums[1] = (sums[1] / clrs.length) | 0
    sums[2] = (sums[2] / clrs.length) | 0
    sums[3] = sums[3] / clrs.length
    return fromrgb(sums[0], sums[1], sums[2], sums[3])
}

// --- removed
