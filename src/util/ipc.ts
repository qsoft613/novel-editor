import { key_key } from './data/keykey'

const _keys: PT.key_key<PT.elecipc> = {
    read_file: 'read_file',
    load_app_option: 'load_app_option',
    load_books: 'load_books',
    load_book: 'load_book',
    pick_dir: 'pick_dir',
    pick_file: 'pick_file',
    pick_files: 'pick_files',
    save_book: '',
}
export const keys: PT.key_key<PT.elecipc> = key_key(_keys)

export function result<T extends any = null>(re?: T): PT.ipcRe<T> {
    return {
        success: true,
        msg: '',
        data: re ?? null,
    }
}

export function result_err<T extends any>(msg?: string, re?: T): PT.ipcRe<T> {
    return {
        success: false,
        msg: msg ?? 'uknown error',
        data: re ?? null,
    }
}

/**
 *
 * @param e
 * @param key
 * @param re
 * @param sync (都有) 'all' | (只同步, 默认) 'sync' | (只异步) 'async'
 */
export function reply<T extends any>(
    e: Electron.IpcMainEvent,
    key: PT.key_key<PT.elecipc>,
    re: PT.ipcRe<T>,
    sync?: 'all' | 'sync' | 'async',
) {
    const _sync = sync ?? 'sync'
    if (_sync === 'all' || _sync === 'sync') {
        e.returnValue = re
    }
    if (_sync === 'all' || _sync === 'async') {
        e.reply(key, re)
    }
}
