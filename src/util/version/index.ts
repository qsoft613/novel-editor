/** 当前版本
 * @readonly
 */
export const cur: PT.version = [3, 0, 0]

export function from(raw: any): PT.version {
    if (!Array.isArray(raw)) {
        return [0, 0, 0]
    }
    const ns3 = raw
        .concat(0, 0, 0)
        .filter((n) => typeof n === 'number')
        .map((n) => {
            n = n | 0
            if (n < 0) {
                n = 0
            }
            return n
        }) as PT.version
    return ns3
}

export function before(v1: PT.version, v2: PT.version) {
    let i = 0
    while (i < 3) {
        if (v1[i] < v2[i]) {
            return true
        }
        if (v1[i] > v2[i]) {
            return false
        }
        i++
    }
    return false
}

export function later(v1: PT.version, v2: PT.version) {
    let i = 0
    while (i < 3) {
        if (v1[i] > v2[i]) {
            return true
        }
        if (v1[i] < v2[i]) {
            return false
        }
        i++
    }
    return false
}

/**
 * 查找合适的版本下标
 * - 优先使用相等版本
 * - 次级向下使用 li 中最接近的版本
 * - 如 li [[1,0,0],[2,0,0]], ver [1,9,9], 应使用 [1,0,0]
 * @param ver
 * @param li
 * @returns
 */
export function find_well_index(ver: PT.version, li: PT.version[]) {
    if (!li.length) {
        return 0
    }
    let i = 0
    while (i < li.length) {
        if (!before(ver, li[i])) {
            i++
        } else {
            i--
            break
        }
    }
    return Math.max(0, Math.min(i, li.length - 1))
}
