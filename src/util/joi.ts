import joi from 'joi'
import * as id from './id'
import * as version from './version'

export function schema_defau_opt() {
    const o: joi.ValidationOptions = {
        stripUnknown: {
            arrays: true,
            objects: false,
        },
        allowUnknown: true,
        abortEarly: false,
    }
    return o
}

export const pub_schemas = {
    version() {
        const it = joi.number().integer().min(0).required().failover(0)
        const arr = joi.array().items(it, it, it).required().failover(version.cur)
        return arr
    },
    id() {
        return joi
            .string()
            .regex(/^[0-9a-z]{32}$/)
            .required()
            .failover(id.mk())
    },
    date_time() {
        return joi
            .string()
            .pattern(/^[0-9]{4}-[0-9]{2}-[0-9]{2}\s[0-9]{2}:[0-9]{2}:[0-9]{2}$/)
            .required()
            .failover('1991-01-04 01:02:03')
    },
}
