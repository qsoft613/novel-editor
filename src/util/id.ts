import { v4 } from 'uuid'

export function mk() {
    return v4().replace(/[^0-9a-z]/g, '')
}
