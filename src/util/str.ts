export function join(...strs: string[]) {
    return strs.filter(Boolean).join(' ')
}
