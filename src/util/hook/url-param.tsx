import { useSearchParams } from 'react-router-dom'

/**
 * 获取url参数
 * - 返回一个对象, key为所有函数参数
 */
export function useUrlParam<K1 extends string>(k1: K1): { [k in K1]: string }
export function useUrlParam<K1 extends string, K2 extends string>(k1: K1, k2: K2): { [k in K1 | K2]: string }
export function useUrlParam<K1 extends string, K2 extends string, K3 extends string>(
    k1: K1,
    k2: K2,
    k3: K3,
): { [k in K1 | K2 | K3]: string }
export function useUrlParam<K1 extends string, K2 extends string, K3 extends string, K4 extends string>(
    k1: K1,
    k2: K2,
    k3: K3,
    k4: K4,
): { [k in K1 | K2 | K3 | K4]: string }
export function useUrlParam(...keys: string[]): Record<string, string> {
    const [sea] = useSearchParams()
    const result = Object.assign({}, null)
    keys.forEach((k) => {
        const v = sea.get(k) || ''
        result[k] = v
    })
    return result
}
