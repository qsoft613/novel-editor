const fs = require('fs')
const path = require('path')

const txt = fs.readFileSync('./raw.local', 'utf-8')
const lines = txt.split('\n')

for (let i = 0; i < lines.length; i += 2) {
    const num = lines[i].trim()
    const clr = lines[i + 1].trim()
    const lll = `--clr-${num}: ${clr};`
    console.log(lll)
}
