declare namespace STYLE {
    type theme =
        | 'rose'
        | 'pink'
        | 'fuchsia'
        | 'purple'
        | 'indigo'
        | 'sky'
        | 'cyan'
        | 'teal'
        | 'green'
        | 'lime'
        | 'orange'
        | 'red'
        | 'gray'
        | 'violet'

    interface themeItem {
        l50: string
        l100: string
        l200: string
        l300: string
        l400: string
        l500: string
        l600: string
        l700: string
        l800: string
        l900: string
        front: string
    }
}
