import { key_key } from '@/util/data'
import { BehaviorSubject } from 'rxjs'

export const cur$ = new BehaviorSubject<STYLE.theme>('sky')

export function use(next: STYLE.theme) {
    cur$.next(next)
    // ---
    const dom_root = document.getElementById('root')!
    const cls = Array.from(dom_root.classList.values())
    // const re = pipe(cls)
    const cls2 = cls.filter((c) => /^theme-[a-z]+$/.test(c))
    dom_root.classList.remove(...cls2)
    dom_root.classList.add(`theme-${next}`)
}

export const items: Record<STYLE.theme, STYLE.themeItem> = {
    /* 紫罗兰 */
    violet: {
        l50: '#f5f3ff',
        l100: '#ede9fe',
        l200: '#ddd6fe',
        l300: '#c4b5fd',
        l400: '#a78bfa',
        l500: '#8b5cf6',
        l600: '#7c3aed',
        l700: '#6d28d9',
        l800: '#5b21b6',
        l900: '#4c1d95',
        front: '#ffffff',
    },
    /*  */
    gray: {
        l50: '#fafafa',
        l100: '#f4f4f5',
        l200: '#e4e4e7',
        l300: '#d4d4d8',
        l400: '#a1a1aa',
        l500: '#71717a',
        l600: '#52525b',
        l700: '#3f3f46',
        l800: '#27272a',
        l900: '#18181b',
        front: '#ffffff',
    },
    /*  */
    red: {
        l50: '#fef2f2',
        l100: '#fee2e2',
        l200: '#fecaca',
        l300: '#fca5a5',
        l400: '#f87171',
        l500: '#ef4444',
        l600: '#dc2626',
        l700: '#b91c1c',
        l800: '#991b1b',
        l900: '#7f1d1d',
        front: '#ffffff',
    },
    /*  */
    orange: {
        l50: '#fff7ed',
        l100: '#ffedd5',
        l200: '#fed7aa',
        l300: '#fdba74',
        l400: '#fb923c',
        l500: '#f97316',
        l600: '#ea580c',
        l700: '#c2410c',
        l800: '#9a3412',
        l900: '#7c2d12',
        front: '#ffffff',
    },
    /* 酸橙 */
    lime: {
        l50: '#f7fee7',
        l100: '#ecfccb',
        l200: '#d9f99d',
        l300: '#bef264',
        l400: '#a3e635',
        l500: '#84cc16',
        l600: '#65a30d',
        l700: '#4d7c0f',
        l800: '#3f6212',
        l900: '#365314',
        front: '#ffffff',
    },
    /*  */
    green: {
        l50: '#f0fdf4',
        l100: '#dcfce7',
        l200: '#bbf7d0',
        l300: '#86efac',
        l400: '#4ade80',
        l500: '#22c55e',
        l600: '#16a34a',
        l700: '#15803d',
        l800: '#166534',
        l900: '#14532d',
        front: '#ffffff',
    },
    /* 水鸭", 短颈野鸭", 蓝绿色 */
    teal: {
        l50: '#f0fdfa',
        l100: '#ccfbf1',
        l200: '#99f6e4',
        l300: '#5eead4',
        l400: '#2dd4bf',
        l500: '#14b8a6',
        l600: '#0d9488',
        l700: '#0f766e',
        l800: '#115e59',
        l900: '#134e4a',
        front: '#ffffff',
    },
    /*  */
    cyan: {
        l50: '#ecfeff',
        l100: '#cffafe',
        l200: '#a5f3fc',
        l300: '#67e8f9',
        l400: '#22d3ee',
        l500: '#06b6d4',
        l600: '#0891b2',
        l700: '#0e7490',
        l800: '#155e75',
        l900: '#164e63',
        front: '#ffffff',
    },
    /*  */
    sky: {
        l50: '#f0f9ff',
        l100: '#e0f2fe',
        l200: '#bae6fd',
        l300: '#7dd3fc',
        l400: '#38bdf8',
        l500: '#0ea5e9',
        l600: '#0284c7',
        l700: '#0369a1',
        l800: '#075985',
        l900: '#0c4a6e',
        front: '#ffffff',
    },
    /* 靛蓝 */
    indigo: {
        l50: '#eef2ff',
        l100: '#e0e7ff',
        l200: '#c7d2fe',
        l300: '#a5b4fc',
        l400: '#818cf8',
        l500: '#6366f1',
        l600: '#4f46e5',
        l700: '#4338ca',
        l800: '#3730a3',
        l900: '#312e81',
        front: '#ffffff',
    },
    /*  */
    purple: {
        l50: '#faf5ff',
        l100: '#f3e8ff',
        l200: '#e9d5ff',
        l300: '#d8b4fe',
        l400: '#c084fc',
        l500: '#a855f7',
        l600: '#9333ea',
        l700: '#7e22ce',
        l800: '#6b21a8',
        l900: '#581c87',
        front: '#ffffff',
    },
    /* 倒挂金钟(灌木，花吊挂，呈红、紫或白色) */
    fuchsia: {
        l50: '#fdf4ff',
        l100: '#fae8ff',
        l200: '#f5d0fe',
        l300: '#f0abfc',
        l400: '#e879f9',
        l500: '#d946ef',
        l600: '#c026d3',
        l700: '#a21caf',
        l800: '#86198f',
        l900: '#701a75',
        front: '#ffffff',
    },
    /*   */
    pink: {
        l50: '#fdf2f8',
        l100: '#fce7f3',
        l200: '#fbcfe8',
        l300: '#f9a8d4',
        l400: '#f472b6',
        l500: '#ec4899',
        l600: '#db2777',
        l700: '#be185d',
        l800: '#9d174d',
        l900: '#831843',
        front: '#ffffff',
    },
    /*   */
    rose: {
        l50: '#fff1f2',
        l100: '#ffe4e6',
        l200: '#fecdd3',
        l300: '#fda4af',
        l400: '#fb7185',
        l500: '#f43f5e',
        l600: '#e11d48',
        l700: '#be123c',
        l800: '#9f1239',
        l900: '#881337',
        front: '#ffffff',
    },
}

/** 主题 */
export const themes = key_key(items)

export function cur_item() {
    return items[cur$.value]
}
