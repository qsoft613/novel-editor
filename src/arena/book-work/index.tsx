import { Util } from '@/util'
import { useEffect } from 'react'
import { useObservable } from 'rxjs-hooks'
import { BlockChapter } from './block-chapter'
import { BlockEditor } from './block-editor'
import * as d from './dv'

export default function BookWork() {
    const book = useObservable(() => d.D.book$)
    const urlps = Util.hook.useUrlParam('src')
    useEffect(() => {
        const catched = d.D.book$.value
        if (!catched) {
            const r = window.electronc.load_book(urlps.src)
            if (r.success) {
                d.D.book$.next(r.data)
            }
        }
    }, [])
    if (!book) {
        return null
    }
    console.log('book===>', book)

    return (
        <div className="h-screen  bg-qy-400">
            <BlockEditor book={book} />
            <BlockChapter book={book} />
        </div>
    )
}
