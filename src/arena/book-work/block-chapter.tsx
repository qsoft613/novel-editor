import { Utilf } from '@/utilf'
import clsx from 'clsx'
import { useEffect, useRef } from 'react'

export function BlockChapter(p: { book: Book.item }) {
    const bl = p.book.work_blocks.find((v) => v.type === 'chapter')
    if (!bl) {
        return null
    }

    return <Wapper block={bl} />
}

function Wapper(p: { block: Book.work_block }) {
    const { block } = p
    return (
        <div
            style={{
                left: Utilf.position.l(block.l),
                right: Utilf.position.r(block.r),
                top: Utilf.position.t(block.t),
                bottom: Utilf.position.b(block.b),
            }}
            className={clsx('absolute bg-qy-100', 'flex flex-col')}
        ></div>
    )
}
