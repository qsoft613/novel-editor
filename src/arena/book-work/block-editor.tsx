import { Utilf } from '@/utilf'
import { useEffect, useRef } from 'react'

export function BlockEditor(p: { book: Book.item }) {
    const li = p.book.work_blocks.filter((v) => v.type === 'editor')
    return (
        <>
            {li.map((bl) => (
                <OneBlock key={bl.id} block={bl} />
            ))}
        </>
    )
}

function OneBlock(p: { block: Book.work_block }) {
    const ref = useRef(null as null | HTMLDivElement)
    const { block } = p
    useEffect(() => {
        const dom = ref.current
        if (!dom) {
            return
        }
        const editor = Utilf.monaco.make(dom)
        return () => {
            editor.dispose()
        }
    }, [])
    return (
        <div
            ref={ref}
            className="absolute border border-solid border-qy-400 "
            style={{
                left: Utilf.position.l(block.l),
                right: Utilf.position.r(block.r),
                top: Utilf.position.t(block.t),
                bottom: Utilf.position.b(block.b),
            }}
        ></div>
    )
}
