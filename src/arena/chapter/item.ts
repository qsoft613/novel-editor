import { Util } from '@/util'

export function mk_volumn(): Book.chapter {
    return {
        v: Util.version.cur,
        children: [],
        id: Util.id.mk(),
        level: 1,
        name: '',
        note: '',
        pid: null,
        _src: '',
        _word_count: 0,
    }
}
export function mk_chapter(vol: Book.chapter): Book.chapter {
    const c = mk_volumn()
    c.pid = vol.id
    c.level = 2
    return c
}

export const keys = Util.data.key_key(mk_volumn)
