declare namespace Book {
    /** 卷 , 章
     * - level 1 卷
     * - level 2 章
     *
     */
    interface chapter extends PT.treeFull<string> {
        v: PT.version
        name: string
        note: string
        _src: string
        _word_count: number
    }
}
