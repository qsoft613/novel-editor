import { Util } from '@/util'
import { useEffect, useState } from 'react'
import * as d_shelf from '@/arena/main-shelf/dv'
import { Rout } from '@/rout'

export default function MainLoad() {
    const [loaded, next_loaded] = useState(false)
    const [res, next_res] = useState(null as null | PT.ipcRe<App.item>)
    useEffect(() => {
        // Util.ipc
        const r = window.electronc.load_app_option()
        next_res(r)
        setTimeout(() => {
            next_loaded(true)
        }, 100)
    }, [])
    if (!loaded) {
        return <Tip txt="加载程序配置..." />
    }
    if (!res.success) {
        return <Tip txt="加载程序配置失败" />
    }
    return <LoadBooks books={res.data.shelf_list} />
}

function LoadBooks(p: { books: string[] }) {
    const [loaded, next_loaded] = useState(false)
    const [res, next_res] = useState(null as null | PT.ipcRe<Book.item[]>)
    useEffect(() => {
        // Util.ipc
        const r = window.electronc.load_books(p.books)
        console.log(r.data)
        d_shelf.books$.next(r.data)

        next_res(r)
        setTimeout(() => {
            next_loaded(true)
            if (r.success) {
                if (r.data.length) {
                    Rout.go2(Rout.target.main.shelf)
                } else {
                    Rout.go2(Rout.target.main.book, {
                        action: 'add',
                    })
                }
                return
            }
        }, 100)
    }, [])
    if (!loaded) {
        return <Tip txt="读取书目..." />
    }
    if (!res.success) {
        return <Tip txt="读取书目失败" />
    }
    return <Tip txt="读取书目完成..." />
}

function Tip(p: { txt: string }) {
    return (
        <div className="overflow-auto">
            <p className="tt-base m-6">{p.txt}</p>
        </div>
    )
}
