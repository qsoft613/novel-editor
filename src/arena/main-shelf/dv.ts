import { BehaviorSubject } from 'rxjs'

export const books$ = new BehaviorSubject<Book.item[]>([])
