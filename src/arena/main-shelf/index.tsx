import { Rout } from '@/rout'
import { Svg } from '@/svg'
import clsx from 'clsx'
import { useEffect } from 'react'
import { useObservable } from 'rxjs-hooks'
import { books$ } from './dv'
import { book$ } from '@/arena/book/dv'

export default function Shelf() {
    const li = useObservable(() => books$, [])
    useEffect(() => {
        const appopt = window.electronc.load_app_option()
        if (!appopt.success) {
            return
        }
        const books = window.electronc.load_books(appopt.data.shelf_list)
        books$.next(books.data)
    }, [])
    return (
        <div>
            <div
                className="box-border grid gap-4 p-4  "
                style={{
                    gridTemplateColumns: 'repeat(auto-fill, minmax(200px, 1fr))',
                }}
            >
                <Add />
                {li.map((book) => (
                    <Item key={book.id} book={book} />
                ))}
            </div>
        </div>
    )
}

function Item(p: { book: Book.item }) {
    const { book } = p
    const cl_li = 'cursor-pointer box-border px-4 transition-all hover:bg-qy-400 hover:pl-8'
    return (
        <div
            className={clsx('group', 'box-border w-full overflow-hidden bg-qy-200', 'relative flex flex-col')}
            style={{
                aspectRatio: '5/6',
            }}
        >
            {/* 封面 */}
            <div
                className={clsx('w-full', 'bg-contain', 'group-hover:hidden')}
                style={{
                    height: 'calc(100% - 36px)',
                    backgroundPosition: 'center 0',
                    backgroundImage: `url(qyimg:///${encodeURI(book.cover)})`,
                    backgroundRepeat: 'no-repeat',
                }}
            ></div>
            {/* 名字 */}
            <div
                className={clsx(
                    'bg-l7 absolute left-0 bottom-0 transition-all ',
                    'h-9 overflow-hidden  group-hover:h-auto ',
                    'text-center text-base',
                    'block items-center justify-center group-hover:flex',
                    'box-border w-full  p-2 ',
                    'overflow-ellipsis whitespace-nowrap group-hover:whitespace-normal',
                )}
                style={{
                    minHeight: '36px',
                }}
            >
                {book.name}
            </div>
            {/* 悬浮出现菜单 */}
            <ul className="absolute left-0 top-0 hidden w-full group-hover:block">
                <li
                    className={cl_li}
                    onClick={() => {
                        book$.next(book)
                        Rout.go2(Rout.target.book.work, {
                            src: book.src,
                        })
                        // util.ipc.send("window_open_editer", book)
                    }}
                >
                    <span className="text-base leading-9 text-qy-front">创作</span>
                </li>
                <li
                    className={cl_li}
                    onClick={() => {
                        const par: RoutParam.main_book_option = {
                            action: 'option',
                            src: p.book.src,
                        }
                        Rout.go2(Rout.target.main.book, par)
                    }}
                >
                    <span className="text-base leading-9 text-qy-front">设置</span>
                </li>
                <li className={cl_li}>
                    <span className="text-base leading-9 text-qy-danger">删除</span>
                </li>
            </ul>
        </div>
    )
}

function Add() {
    return (
        <div
            className={clsx('cursor-pointer bg-qy-200', 'flex items-center justify-center', 'group')}
            style={{
                aspectRatio: '5/6',
            }}
            onClick={() => {
                const par: RoutParam.main_book_option = {
                    action: 'add',
                    src: '',
                }
                Rout.go2(Rout.target.main.book, par)
            }}
        >
            <div className={clsx('opacity-70 group-hover:opacity-100')}>
                <Svg.Graph.Add.Ghost len={60} />
            </div>
        </div>
    )
}
