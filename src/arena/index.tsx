import { Rout } from '@/rout'
import React, { useEffect } from 'react'
import { HashRouter, Outlet, Route, Routes, useNavigate } from 'react-router-dom'

const LazyMainRouts = {
    [Rout.target.main.shelf]: React.lazy(() => import('./main-shelf')),
    [Rout.target.main.load]: React.lazy(() => import('./main-load')),
    [Rout.target.main.book]: React.lazy(() => import('./book-option')),
}
const LazyBookRouts = {
    [Rout.target.book.work]: React.lazy(() => import('./book-work')),
}

export function Arena() {
    return (
        <>
            <HashRouter>
                <RouteBox />
            </HashRouter>
        </>
    )
}

function RouteBox() {
    const go = useNavigate()
    useEffect(() => {
        const ob = Rout._unsafe_goto$.subscribe((rt) => {
            if (rt) {
                go(rt)
            }
        })
        return () => {
            ob.unsubscribe()
        }
    }, [])
    // const ur = user
    return (
        <Routes>
            <Route path={Rout.l1.main} element={<Outlet />}>
                {Object.entries(LazyMainRouts).map((rt_node) => {
                    const N = rt_node[1]
                    const path = rt_node[0]
                    return (
                        <Route
                            key={path}
                            path={path}
                            element={
                                <React.Suspense fallback={null}>
                                    <N />
                                </React.Suspense>
                            }
                        />
                    )
                })}
            </Route>
            <Route path={Rout.l1.book} element={<Outlet />}>
                {Object.entries(LazyBookRouts).map((rt_node) => {
                    const N = rt_node[1]
                    const path = rt_node[0]
                    return (
                        <Route
                            key={path}
                            path={path}
                            element={
                                <React.Suspense fallback={null}>
                                    <N />
                                </React.Suspense>
                            }
                        />
                    )
                })}
            </Route>
            <Route index element={<T401 />} />
        </Routes>
    )
}

function T401(): null {
    useEffect(() => {
        const old = window.location.href
        const next = old.replace(/main_window.*$/, 'main_window/index.html#' + Rout.target.main.load)
        window.location.href = next
    }, [])

    return null
}
