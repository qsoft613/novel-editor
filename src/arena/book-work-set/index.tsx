import React from 'react'

export default function BookWorkSet() {
    return <div className="h-6 bg-qy-300"></div>
}

interface Level1P {
    node: PT.treeLike
    expander: boolean
    height?: number | string

    suffNode?: React.ReactNode
}
function Level1(p: Level1P) {
    return (
        <div
            className="flex items-center "
            style={{
                height: p.height ?? 32,
            }}
        >
            {p.suffNode}
        </div>
    )
}
