import { Util } from '@/util'

export function mk(): App.item {
    return {
        shelf_list: [],
        ui_theme: 'word',
        v: Util.version.cur,
    }
}

export const keys = Util.data.key_key(mk)
