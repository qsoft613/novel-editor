/** 应用程序 */
declare namespace App {
    type item = item_v3
    interface item_v3 {
        v: PT.version
        /** 书目列表 fs.path[] */
        shelf_list: string[]
        /** 主题 */
        ui_theme: 'word' | 'excel' | 'ppt'
    }
}
