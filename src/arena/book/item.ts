import { Util } from '@/util'

export function mk(): Book.item {
    return {
        v: Util.version.cur,
        action: 'write',
        id: Util.id.mk(),
        name: '',
        src: '',
        _sort: 0,
        cover: '',
        edit_last_chapter: '',
        edit_last_time: '1991-01-04 01:02:03',
        edit_last_20_chapter: [],
        _well: false,
        editer_size_max: false,
        work_blocks: [mk_work_block('editor'), mk_work_block('chapter')],
    }
}

export const keys = Util.data.key_key(mk)

export function mk_work_block(type: Book.work_block['type']) {
    const re: Book.work_block = {
        type: type,
        editor_chapter: null,
        id: Util.id.mk(),
        l: 200,
        r: 200,
        b: 200,
        t: 200,
    }
    switch (type) {
        case 'chapter':
            re.l = 0
            re.r = -200
            re.t = 0
            re.b = 0
            break
        default:
            break
    }
    return re
}
