import { BehaviorSubject } from 'rxjs'

export const book$ = new BehaviorSubject(null as null | Book.item)
