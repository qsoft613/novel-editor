declare namespace Book {
    type item = item_v3

    interface item_v3 {
        v: PT.version
        /**
         * - add 新增
         * - option 修改配置
         * - write 编写小说内容
         */
        action: 'add' | 'option' | 'write'

        id: string
        name: string
        src: string
        _sort: number

        /** 封面 */
        cover: string
        /** 最后编辑的章节 */
        edit_last_chapter: string
        /** YYYY-MM-DD HH:mm:ss */
        edit_last_time: string
        /** 最近20章 id[] */
        edit_last_20_chapter: string[]
        editer_size_max: boolean
        /**
         * 标准化后自动修改
         * false 读取出错 */
        _well: boolean
        /** 工作区块 */
        work_blocks: work_block[]
    }
    /** 一个工作区块
     * - 编写框可以多个, 其他单个
     */
    interface work_block {
        id: string
        /**
         * 块类型
         * - editor 编辑
         * - chapter 章节目录
         * - word 词
         * - outline 大纲
         * - timeline 时间线
         * - npc 角色
         *
         */
        type: 'editor' | 'chapter' | 'word' | 'outline' | 'timeline' | 'npc'
        /** 左 */
        l: number
        /** 右 */
        r: number
        /** 上 */
        t: number
        /** 下 */
        b: number
        /** editor 时章 */
        editor_chapter: null | any
    }
}
