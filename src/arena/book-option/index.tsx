import { Rout } from '@/rout'
import { Svg } from '@/svg'
import { Ui } from '@/ui'
import { Util } from '@/util'
import { take } from 'rxjs'
import { useObservable } from 'rxjs-hooks'
import * as d from './dv'

export default function BookOption() {
    const urlpar = Util.hook.useUrlParam('src', 'action')
    const resol: RoutParam.main_book_option = {
        action: urlpar.action as any,
        src: urlpar.src,
    }

    if (resol.action === 'add') {
        // 添加书
        return <FormShow />
    } else if (resol.action === 'option') {
        // 编辑书
        const res = window.electronc.load_book(resol.src)

        if (res.success) {
            d.ctrl.merge((fv) => {
                Object.assign(fv, res.data)
            })
            return <FormShow />
        } else {
            return null
        }
    } else {
        // 编写书不可能出现
        return null
    }
}

function FormShow() {
    return (
        <>
            <section
                className="relative z-10 mt-10 ml-10 space-y-3"
                style={{
                    width: 500,
                }}
            >
                <Ui.Groups.Text ctrl={d.ctrl} field={d.keys.name} />
                <Ui.Groups.Text
                    ctrl={d.ctrl}
                    field={d.keys.src}
                    inputerOption={{
                        placeholder: '选择目录地址',
                    }}
                    rt={{
                        node: (
                            <div
                                className="qy-click-able flex h-full w-full items-center justify-center"
                                onClick={() => {
                                    const src = window.electronc.pick_dir()
                                    d.ctrl.merge((fv) => {
                                        fv.src = src.data
                                    })
                                }}
                            >
                                <Svg.Graph.Dir.Base len={24} />
                            </div>
                        ),
                        width: 36,
                    }}
                />
                <Ui.Groups.Text
                    ctrl={d.ctrl}
                    field={d.keys.cover}
                    rt={{
                        node: (
                            <div
                                className="qy-click-able flex h-full w-full items-center justify-center"
                                onClick={() => {
                                    const src = window.electronc.pick_file(['png', 'jpg', 'svg'])
                                    d.ctrl.merge((fv) => {
                                        fv.cover = src.data
                                    })
                                }}
                            >
                                <Svg.Graph.Dir.Base len={24} />
                            </div>
                        ),
                        width: 36,
                    }}
                />
                <div className="flex justify-end space-x-4 pt-10">
                    <Ui.Buttons.Cancel
                        onClick={() => {
                            // d.ctrl.check()
                            Rout.go2(Rout.target.main.shelf)
                        }}
                        width={120}
                    />

                    <Ui.Buttons.Primary
                        onClick={() => {
                            const cr = d.ctrl.check_all()
                            if (!cr.well) {
                                return
                            }
                            const r = window.electronc.save_book(d.ctrl.now())
                            console.log(r)
                            if (r.success) {
                                Rout.go2(Rout.target.main.shelf)
                            }
                        }}
                        width={120}
                    />
                </div>
            </section>
            <CoverPreview />
        </>
    )
}

function CoverPreview() {
    const a = useObservable(() => d.ctrl.observe_field$(d.keys.cover), null)
    const src = a?.value
    if (!src) {
        return null
    }
    return (
        <div
            className="absolute right-10 top-10 h-96 w-96 bg-contain"
            style={{
                backgroundPosition: '100% 0',
                backgroundImage: `url(qyimg:///${encodeURI(src)})`,
                backgroundRepeat: 'no-repeat',
            }}
        ></div>
    )
}
