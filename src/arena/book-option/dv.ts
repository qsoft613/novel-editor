import { NOUI } from '@/noui'
import { BehaviorSubject } from 'rxjs'
import { mk, keys } from '@/arena/book/item'

export const ctrl = NOUI.form.make({
    initer: mk,
    field_show: {
        cover: '封面',
        src: '地址',
        name: '书名',
    },
    disables: [],
    requires: [keys.src, keys.name],

    formatters: [
        {
            field: keys.name,
            preset: 'trim',
        },
    ],
})

export { keys } from '@/arena/book/item'

export function mount() {
    ctrl.init()
}
