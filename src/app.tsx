import { createRoot } from 'react-dom/client'
import 'tailwindcss/tailwind.css'
import { Arena } from './arena'
import './style'
import { Util } from './util'

window.electronc.publish((arg) => {})
createRoot(document.getElementById('root')!, {}).render(<Arena />)
