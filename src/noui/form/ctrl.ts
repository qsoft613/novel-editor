import { Util } from '@/util'
import produce from 'immer'
import { BehaviorSubject, debounceTime, distinctUntilChanged, map, Subscription, switchMap } from 'rxjs'

interface makeP<Form extends NOUI.form_val, Field extends NOUI.form_pick_field<Form> = string> {
    initer(): Form
    requires?: NOUI.form_pick_field<Form>[]
    disables?: NOUI.form_pick_field<Form>[]
    rules?: { [f in NOUI.form_pick_field<Form>]: NOUI.checker<Form>[] }
    /** 字段展示方式 */
    field_show?: { [f in NOUI.form_pick_field<Form>]?: string }
    filters?: NOUI.form_input_set_filter_param<Field>[]
    formatters?: NOUI.form_input_set_formatter_param<Field>[]
}

export function make<Form extends NOUI.form_val, Field extends NOUI.form_pick_field<Form> = string>(
    p: makeP<Form, Field>,
): NOUI.form<Form> {
    const initer = p.initer
    const _init_val = initer()
    const requires = new Map<NOUI.form_pick_field<Form>, boolean>((p.requires ?? []).map((k) => [k, true]))
    const check_requires$ = new BehaviorSubject(requires)
    const disables = new Map<NOUI.form_pick_field<Form>, boolean>((p.disables ?? []).map((v) => [v, true]))
    const disables$ = new BehaviorSubject(disables)
    const rules = new Map<NOUI.form_pick_field<Form>, NOUI.checker<Form>[]>(Object.entries(p.rules ?? {}))
    const check_rules$ = new BehaviorSubject(rules)
    const errs = new Map<NOUI.form_pick_field<Form>, string>(Object.keys(_init_val).map((k) => [k, '']))
    const check_errs$ = new BehaviorSubject(errs)
    const val$ = new BehaviorSubject(_init_val)
    const field_filter = new Map<Field, (raw: string) => string>()
    const field_formatter = new Map<Field, Subscription>()
    const anemia: NOUI.form_anemia<Form> = {
        val$,
        check_requires$,
        check_rules$,
        check_errs$,
        disables$,
        field_show: help_field_show<Form, Field>(_init_val, p.field_show),
        field_filter,
        field_formatter,
    }
    const full: NOUI.form<Form, Field> = {
        now() {
            return anemia.val$.value
        },
        field_show(field) {
            return anemia.field_show[field]
        },
        observe_all$() {
            return anemia.val$.pipe()
        },
        observe_field$(field) {
            return help_observe_field$(anemia, field)
        },
        observe_pick$(picker) {
            return val$.pipe(
                map(picker),
                distinctUntilChanged(),
                map((value) => {
                    return {
                        value,
                        err: '',
                        disabled: false,
                        required: false,
                        show: '',
                    }
                }),
            )
        },
        check_all() {
            return help_check_all(anemia)
        },
        check_field(field) {
            return {
                err: '',
                well: true,
            }
        },
        merge(fun) {
            help_merge(anemia, fun)
        },
        init() {
            val$.next(initer())
        },
        input_set_filter(p) {
            help_input_set_filter(anemia, p)
        },
        input_set_formatter(p) {
            help_input_set_formatter(anemia, p)
        },
    }
    if (p.filters) {
        p.filters.forEach((par) => {
            full.input_set_filter(par)
        })
    }
    if (p.formatters) {
        p.formatters.forEach((par) => {
            full.input_set_formatter(par)
        })
    }
    return full
}

/** 观察一个字段 */
function help_observe_field$<Form extends Record<string, any>, Field extends NOUI.form_pick_field<Form>>(
    anemia: NOUI.form_anemia<Form>,
    field: Field,
) {
    const { val$, disables$, field_show } = anemia
    const errs$ = anemia.check_errs$
    const requires$ = anemia.check_requires$

    const field_val$ = val$.pipe(
        map((fv) => fv[field]),
        distinctUntilChanged(),
    )
    const field_err$ = errs$.pipe(
        map((errs) => {
            return errs.get(field) || ''
        }),
        distinctUntilChanged(),
    )
    const field_disabled$ = disables$.pipe(
        map((mp) => mp.get(field) ?? false),
        distinctUntilChanged(),
    )
    const field_required$ = requires$.pipe(
        map((mp) => Boolean(mp.get(field))),
        distinctUntilChanged(),
    )
    return field_val$.pipe(
        switchMap((value) => field_err$.pipe(map((err) => ({ value, err })))),
        switchMap((o) => field_disabled$.pipe(map((disabled) => ({ ...o, disabled })))),
        switchMap((o) => field_required$.pipe(map((required) => ({ ...o, required })))),
        map((o) => {
            const maybe_fieldshow = field_show[field]
            const show = maybe_fieldshow ?? field
            return {
                ...o,
                show,
            }
        }),
    )
}

function help_field_show<Form extends Record<string, any>, Field extends NOUI.form_pick_field<Form>>(
    val: Form,
    part: { [f in Field]?: string },
): { [f in Field]: string } {
    val = Object.assign({}, val)
    part = Object.assign({}, part)
    const re: any = {}
    const keys = Object.keys(val) as Field[]
    keys.forEach((k) => {
        re[k] = part[k] ?? k
    })
    return re
}

function help_check_all<Form extends Record<string, any>, Field extends NOUI.form_pick_field<Form>>(
    anemia: NOUI.form_anemia<Form>,
): NOUI.form_checkre {
    const { val$, disables$, field_show } = anemia
    const errs$ = anemia.check_errs$
    const requires$ = anemia.check_requires$
    const rules$ = anemia.check_rules$

    const errs: Map<Field, string> = new Map()
    const form_val = val$.value
    let well = true
    const fields = Object.keys(form_val) as Field[]
    const requires = requires$.value

    fields.forEach((fie) => {
        const disable = disables$.value.get(fie) ?? false
        if (disable) {
            return
        }
        const val = form_val[fie]
        const rules = rules$.value.get(fie) || []
        for (const rule of rules) {
            const checkre = rule(val)
            if (checkre.well === false) {
                errs.set(fie, checkre.err)
                well = false
                return
            }
        }
        if (requires.get(fie)) {
            console.log('fie', fie, val)

            if (val !== 0 && !Boolean(val)) {
                errs.set(fie, `${field_show[fie]}是必填的`)
                well = false
                return
            }
        }
    })
    errs$.next(errs)
    return {
        well,
        errs,
    }
}

function help_merge<Form extends Record<string, any>, Field extends NOUI.form_pick_field<Form>>(
    anemia: NOUI.form_anemia<Form>,
    fun: (fv: Form) => void,
): void {
    const { val$ } = anemia
    const next = Util.data.pipe(
        val$.value,
        // 1
        (old) => produce(old, fun),
        // 2
        (fv) =>
            produce(fv, (fv) => {
                const fies = pick_fields(fv)
                fies.forEach((fie) => {
                    const f = anemia.field_filter.get(fie)
                    if (f) {
                        fv[fie] = f(fv[fie]) as any
                    }
                })
            }),
    )
    val$.next(next)
}

function help_input_set_filter<Form extends Record<string, any>, Field extends NOUI.form_pick_field<Form>>(
    anemia: NOUI.form_anemia<Form>,
    p: NOUI.form_input_set_filter_param<Field>,
) {
    const fie = p.field
    const m = anemia.field_filter
    if (p.filter) {
        m.set(fie, p.filter)
        return
    }
    if (p.preset) {
        switch (p.preset) {
            case 'int':
                m.set(fie, (raw) => raw.replace(/[^0-9]/g, ''))
                break
            case 'no_empty':
                m.set(fie, (raw) => raw.replace(/\s/g, ''))
                break

            default:
                break
        }
        return
    }
    if (p.reg === undefined && p.length === undefined) {
        return
    }

    m.set(fie, (raw) => {
        let re: string = raw
        if (p.reg) {
            re = re
                .split('')
                .filter((lt) => p.reg.test(lt))
                .join('')
        }
        if (p.length) {
            re = re.slice(0, p.length)
        }
        return re
    })
}
function help_input_set_formatter<Form extends Record<string, any>, Field extends NOUI.form_pick_field<Form>>(
    anemia: NOUI.form_anemia<Form>,
    p: NOUI.form_input_set_formatter_param<Field>,
) {
    const fie = p.field
    const m = anemia.field_formatter
    if (!p.preset && !p.formatter) {
        return
    }

    const sub = anemia.val$
        .pipe(
            map((fv) => fv[fie] as string),
            distinctUntilChanged(),
            debounceTime(p.timeout ?? 1000),
        )
        .subscribe((fieval) => {
            const next_form_val = produce(anemia.val$.value, (fv) => {
                if (p.formatter) {
                    fv[fie] = p.formatter(fieval) as any
                    return
                }
                if (p.preset) {
                    switch (p.preset) {
                        case 'trim':
                            fv[fie] = fieval.trim() as any
                            break

                        default:
                            break
                    }
                }
            })
            anemia.val$.next(next_form_val)
        })
    m.set(fie, sub)
}

function pick_fields<Form extends Record<string, any>, Field extends NOUI.form_pick_field<Form>>(form: Form): Field[] {
    const ks = Object.keys(form) as Field[]
    return ks
}
