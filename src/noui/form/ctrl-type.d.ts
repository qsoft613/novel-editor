import { BehaviorSubject, Observable } from 'rxjs'

declare global {
    declare namespace NOUI {
        /** 表单值 */
        type form_val = Record<string, any>
        /** 贫血表单, 只有属性 */
        interface form_anemia<Form extends Record<string, any>, Field extends NOUI.form_pick_field<Form> = string> {
            val$: BehaviorSubject<Form>
            disables$: BehaviorSubject<Map<Field, boolean>>
            check_requires$: BehaviorSubject<Map<Field, boolean>>
            check_errs$: BehaviorSubject<Map<Field, string>>
            check_rules$: BehaviorSubject<Map<Field, NOUI.checker<Form>[]>>
            /** 字段展示文字 */
            field_show: { [fie in Field]: string }
            field_filter: Map<Field, (raw: string) => string>
            field_formatter: Map<Field, any>
        }

        /** 充血表单, 带有方法 */
        interface form<Form extends Record<string, any>, Field extends NOUI.form_pick_field<Form> = string> {
            /** 字段的展示标签 */
            field_show(field: Field): string
            /** 订阅表单值 */
            observe_all$(): Observable<Form>
            /** 对单个字段进行订阅 */
            observe_field$<Field extends NOUI.form_pick_field<Form>>(field: Field): Observable<field_about<Form, Field>>
            /** 对取值方法进行订阅 */
            observe_pick$<Value = string>(picker: (form: Form) => Value): Observable<field_about<Form, Field, Value>>
            /** 输入时, 对字段的值进行过滤
             * - 仅生效一项, 但regexp和length可以同时生效
             * - 优先级 filter > preset > reg | length
             * - reg是对每个字符逐个检查
             */
            input_set_filter(p: form_input_set_filter_param<Field>): void
            /** 输入一段时间后, 对字段的值进行格式化
             * - 仅生效一项, 但regexp和length可以同时生效
             * - 优先级 formatter > preset
             * - reg是对每个字符逐个检查
             */
            input_set_formatter(p: form_input_set_formatter_param<Field>): void
            check_all(): form_checkre
            check_field(field: Field): form_field_checkre
            merge(fun: (fv: Form) => void): void
            init(): void
            now(): Form
        }
        interface form_input_set_filter_param<Field extends NOUI.form_pick_field<Form> = string> {
            field: Field
            preset?: 'no_empty' | 'int' | 'float' | 'phone' | 'email'
            reg?: RegExp
            length?: number
            filter?(raw: string): string
        }
        interface form_input_set_formatter_param<Field extends NOUI.form_pick_field<Form> = string> {
            field: Field
            preset?: 'trim' | 'int' | 'float' | 'phone' | 'email'
            formatter?(raw: string): string
            /** x毫秒后执行, 必须大于0
             * - 默认 1000 ms
             */
            timeout?: number
        }
        /** 某个字段的值 */
        interface field_about<Form extends Record<string, any>, Field extends NOUI.form_pick_field<Form>> {
            /** 值 */
            value: Form[Field]
            err: string
            disabled: boolean
            required: boolean
            /** 展示给用户看的文字 */
            show: string
        }

        interface form_checkre {
            well: boolean
            errs: Map<Field, string>
        }
        interface form_field_checkre {
            well: boolean
            err: string
        }

        /** 获取表单的字段 */
        type form_pick_field<Form extends Record<string, any>> = keyof Form & string
        /** 检查
         * - 输入 整个表单 */
        type checker<Form extends Record<string, any>> = (s: Form) => check_re
        /**
         * 检查
         * - 输入 单个字段
         */
        type checker_val = (s: any) => check_re

        interface check_re {
            well: boolean
            err: string
        }
    }
}
