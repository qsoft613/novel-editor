interface P {
    /** 24 */
    len?: number
    /** gray */
    color_preset?: 'gray' | 'blue'
    /** 1 */
    opacity?: number
}
export function Base(p: P) {
    const len = p.len ?? 24
    const color = hand_color(p)
    const opacity = p.opacity ?? 1
    return (
        <svg
            width={len}
            height={len}
            viewBox="0 0 24 24"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            opacity={opacity}
        >
            <path
                d="M12 21C16.9706 21 21 16.9706 21 12C21 7.02944 16.9706 3 12 3C7.02944 3 3 7.02944 3 12C3 16.9706 7.02944 21 12 21Z"
                stroke={color}
                strokeWidth="2"
                strokeMiterlimit="10"
                strokeLinecap="round"
                strokeLinejoin="round"
            />
            <path
                d="M9 9L14.9999 14.9999"
                stroke={color}
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
            />
            <path
                d="M9 15L14.9999 9.00013"
                stroke={color}
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
            />
        </svg>
    )
}

function hand_color(p: P) {
    const preset = p.color_preset ?? 'gray'
    switch (preset) {
        case 'gray':
            return '#030B20'
        case 'blue':
            return 'rgb(42,101,253)'
        default:
            break
    }
}
