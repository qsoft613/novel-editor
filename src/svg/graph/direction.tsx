import { Util } from '@/util'
import clsx from 'clsx'

interface BaseP {
    /** 优先级 > color_preset */
    color?: Color.item
    color_preset?: 'gray' | 'white' | 'black'
    /** 15 */
    len?: number
    /** right */
    arrowTo?: 'right' | 'left' | 'top' | 'bottom'
}

export function Base(p: BaseP) {
    const clr = hand_color(p)
    const h = p.len ?? 15
    const w = ((h / 15) * 9) | 0
    const arrowTo = p.arrowTo ?? 'right'
    return (
        <div
            style={{
                width: w,
                height: h,
            }}
            className={clsx({
                '-rotate-90': arrowTo === 'top',
                'rotate-90': arrowTo === 'bottom',
                'rotate-180': arrowTo === 'left',
            })}
        >
            <svg width={w} height={h} viewBox="0 0 9 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M8.57107 7.75757C8.62469 7.44798 8.53197 7.118 8.29291 6.87895L2.63606 1.22209C2.24554 0.831568 1.61237 0.831568 1.22185 1.22209C0.831324 1.61262 0.831324 2.24578 1.22185 2.63631L6.34316 7.75762L1.22186 12.8789C0.831336 13.2694 0.831336 13.9026 1.22186 14.2931C1.61238 14.6837 2.24555 14.6837 2.63607 14.2931L8.29293 8.63627C8.53201 8.3972 8.62472 8.06718 8.57107 7.75757Z"
                    fill={clr.string()}
                />
            </svg>
        </div>
    )
}

function hand_color(p: BaseP) {
    let clr = Util.colors.fromrgb(3, 11, 32)
    switch (p.color_preset) {
        case 'black':
            break
        case 'gray':
            clr = Util.colors.fromrgb(196, 196, 196)
            break
        case 'white':
            clr = Util.colors.fromrgb(255, 255, 255)
            break
        default:
            break
    }
    if (p.color) {
        clr = p.color
    }
    return clr
}
