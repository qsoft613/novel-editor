interface P {
    /** 24 */
    len?: number
    /** gray */
    color_preset?: 'gray' | 'blue'
    /** 1 */
    opacity?: number
}

export function Base(p: P) {
    const len = p.len ?? 42

    return (
        <svg width={len} height={len} viewBox="0 0 42 42" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="21" cy="21" r="21" fill="#2A65FD" />
            <rect x="14" y="20.2227" width="14" height="2" rx="1" fill="white" />
            <rect x="21.7773" y="14" width="14" height="2" rx="1" transform="rotate(90 21.7773 14)" fill="white" />
        </svg>
    )
}

export function Ghost(p: P) {
    const len = p.len ?? 42

    return (
        <svg width={len} height={len} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="12" cy="12" r="9" stroke="#2A65FD" strokeWidth="2" />
            <rect x="7" y="10.999" width="10" height="2" rx="1" fill="#2A65FD" />
            <rect x="11" y="17" width="10" height="2" rx="1" transform="rotate(-90 11 17)" fill="#2A65FD" />
        </svg>
    )
}
