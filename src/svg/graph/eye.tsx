import { Util } from '@/util'

interface BaseP {
    /** 边长 */
    len?: number
    /** 颜色
     * - 默认灰色
     * - color > color_preset
     */
    color?: Color.item
    /** 预设颜色 */
    color_preset?: 'white' | 'gray'
}
export function Open(p: BaseP) {
    const len = p.len ?? 20
    const rgb = _hand_color(p)

    return (
        <svg width={len} height={len} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path
                d="M11.9994 6C7.69989 6 4.30035 8.0004 2 12C4.30035 15.9996 7.69989 18 11.9994 18C16.2989 18 19.6996 15.9996 21.9988 12C19.6996 8.0004 16.3001 6 11.9994 6ZM12.1998 15.9996C9.19986 15.9996 6.19992 14.7 4.49955 12C6.19992 9.3 9.19986 8.0004 12.1998 8.0004C15.1997 8.0004 17.8001 9.3 19.4993 12C17.8001 14.7 15.1997 15.9996 12.1998 15.9996ZM11.9994 9C10.299 9 8.99946 10.2996 8.99946 12C8.99946 13.7004 10.299 15 11.9994 15C13.6998 15 14.9993 13.7004 14.9993 12C14.9993 10.2996 13.6998 9 11.9994 9ZM11.9994 11.0004C12.5994 11.0004 12.999 11.4 12.999 12C12.999 12.6 12.5994 12.9996 11.9994 12.9996C11.3994 12.9996 10.9998 12.6 10.9998 12C10.9998 11.4 11.3994 11.0004 11.9994 11.0004Z"
                fill={rgb}
            />
        </svg>
    )
}

export function Close(p: BaseP) {
    const len = p.len ?? 20
    const rgb = _hand_color(p)
    return (
        <svg width={len} height={len} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path
                d="M4.6748 5L18.0748 18.4"
                stroke={rgb}
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
            />
            <path
                d="M15.1437 15.6832C14.011 16.2934 12.7306 16.725 11.375 16.725C6.74961 16.725 3 11.7 3 11.7C3 11.7 4.74905 9.35608 7.33159 7.86991M17.6562 13.9057C18.9593 12.7597 19.75 11.7 19.75 11.7C19.75 11.7 16.0004 6.67505 11.375 6.67505C11.0923 6.67505 10.813 6.69381 10.5375 6.72905"
                stroke={rgb}
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
            />
            <path
                d="M12.4831 12.9562C12.1879 13.2168 11.8 13.3749 11.3752 13.3749C10.4501 13.3749 9.7002 12.625 9.7002 11.6999C9.7002 11.2481 9.8791 10.838 10.1699 10.5367"
                stroke={rgb}
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
            />
        </svg>
    )
}

function _hand_color(p: BaseP) {
    const pres: Record<string, Color.item> = {
        gray: Util.colors.fromhex('#030B20').alpha(0.5),
        white: Util.colors.fromhex('#FFFFFF'),
    }
    const clr = pres[p.color_preset || ''] ?? p.color ?? pres.gray

    return clr.string()
}
