import { Util } from '@/util'
interface BaseP {
    /** 边长 */
    len?: number
    /** 开头较深的颜色 */
    color_start: Color.item
    /** 末尾较浅的颜色
     * - 默认为 color1 的 alpha 0
     */
    color_end?: Color.item
}
export function Base(p: BaseP) {
    const len = p.len ?? 20
    const rgbs = _hand_colors(p)

    return (
        <svg className="animate-spin" width={len} height={len} viewBox="0 0 440 440" xmlns="http://www.w3.org/2000/svg">
            <defs>
                <linearGradient x1="1" y1="0" x2="0" y2="0" id="gradient1">
                    <stop offset="0%" stopColor={rgbs[0]}></stop>
                    <stop offset="100%" stopColor={rgbs[2]}></stop>
                </linearGradient>
                <linearGradient x1="1" y1="0" x2="0" y2="0" id="gradient2">
                    <stop offset="0%" stopColor={rgbs[1]}></stop>
                    <stop offset="100%" stopColor={rgbs[0]}></stop>
                </linearGradient>
            </defs>
            <g transform="matrix(0,-1,1,0,0,440)">
                <circle cx="220" cy="220" r="170" strokeWidth="50" fill="none" strokeDasharray="1069 1069"></circle>
                <circle
                    cx="220"
                    cy="220"
                    r="170"
                    strokeWidth="50"
                    stroke="url('#gradient1')"
                    fill="none"
                    strokeDasharray="1069 1069"
                ></circle>
                <circle
                    cx="220"
                    cy="220"
                    r="170"
                    strokeWidth="50"
                    stroke="url('#gradient2')"
                    fill="none"
                    strokeDasharray="534.5 1069"
                ></circle>
            </g>
        </svg>
    )
}

function _hand_colors(p: BaseP) {
    const color_start = p.color_start
    const color_end = p.color_end ?? p.color_start.alpha(0)
    const color_mid = Util.colors.mid(color_start, color_end)

    const rgbs = [color_start, color_mid, color_end].map((c) => c.string())
    return rgbs
}
