/** 原始路由
 * - 一般没用
 * - 1级是系统
 * - 2级是模块
 */
export const raw = {
    /** 碎片页, 没归类 */
    main: {
        /** 书架 */
        shelf: '',
        /** 书 新增 | 编辑 */
        book: '',
        /** 程序配置 */
        option: '',
        /** 加载页 */
        load: '',
    },
    book: {
        /** 工作区页 */
        work: '',
        /** 工作区配置 */
        workset: '',
        /** 角色 */
        npc: '',
        npcset: '',
        /** 词条 */
        word: '',
        wordset: '',
        /** 时间线 */
        timeline: '',
        timelineset: '',
        /** 大纲 */
        outline: '',
        outlineset: '',
    },
}
