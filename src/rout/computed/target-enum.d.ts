declare namespace ROUT {
    type target_enum =
        | '/main/shelf'
        | '/main/book'
        | '/main/option'
        | '/main/load'
        | '/book/work'
        | '/book/workset'
        | '/book/npc'
        | '/book/npcset'
        | '/book/word'
        | '/book/wordset'
        | '/book/timeline'
        | '/book/timelineset'
        | '/book/outline'
        | '/book/outlineset'
}
