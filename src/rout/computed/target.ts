export const target: ROUT.target = {
    main: { shelf: '/main/shelf', book: '/main/book', option: '/main/option', load: '/main/load' },
    book: {
        work: '/book/work',
        workset: '/book/workset',
        npc: '/book/npc',
        npcset: '/book/npcset',
        word: '/book/word',
        wordset: '/book/wordset',
        timeline: '/book/timeline',
        timelineset: '/book/timelineset',
        outline: '/book/outline',
        outlineset: '/book/outlineset',
    },
}
