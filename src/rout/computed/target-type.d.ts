/** 原始路由
 * - 一般没用
 * - 1级是系统
 * - 2级是模块
 */
declare namespace ROUT {
    interface target {
        /** 碎片页, 没归类 */
        main: {
            /** 书架 */
            shelf: '/main/shelf'
            /** 书 新增 | 编辑 */
            book: '/main/book'
            /** 程序配置 */
            option: '/main/option'
            /** 加载页 */
            load: '/main/load'
        }
        book: {
            /** 工作区页 */
            work: '/book/work'
            /** 工作区配置 */
            workset: '/book/workset'
            /** 角色 */
            npc: '/book/npc'
            npcset: '/book/npcset'
            /** 词条 */
            word: '/book/word'
            wordset: '/book/wordset'
            /** 时间线 */
            timeline: '/book/timeline'
            timelineset: '/book/timelineset'
            /** 大纲 */
            outline: '/book/outline'
            outlineset: '/book/outlineset'
        }
    }
}
