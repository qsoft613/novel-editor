import { Subject } from 'rxjs'
import { stringify } from 'qs'

export const _unsafe_goto$ = new Subject<string>()

/** router顶层的路由跳转, 方便js控制 */
export function go2<Par extends Record<string, string>>(to: ROUT.target_enum, param?: Par) {
    const p = param ? `?${stringify(param)}` : ''
    const full = to + p
    _unsafe_goto$.next(full)
}

/** 回退 */
export function back(deep = -1) {
    _unsafe_goto$.next(deep as any)
}

/** 不入栈更新地址 */
export function replace(to: ROUT.target_enum, param?: Record<string, string>) {
    const p = param ? `?${stringify(param)}` : ''
    const full = '/#' + to + p

    window.location.replace(full)
}
