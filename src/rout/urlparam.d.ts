/** 路由参数 */
declare namespace RoutParam {
    interface main_book_option extends Record<string, string> {
        src: string
        action: Book.item['action']
    }
}
