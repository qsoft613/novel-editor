import path from 'path'
import fs from 'fs'
import { raw } from './raw.mjs'

const __dirname = path.resolve()

// console.log(raw)

const def_txt = fs.readFileSync(path.join(__dirname, 'raw.mjs'), 'utf-8')

// --- target type
const txt_type = make_txt_type(def_txt)

/**
 *
 * @param {string} txt0
 */
function make_txt_type(txt0) {
    const lines = txt0
        .split('\n')
        .map((s) => s.trim())
        .filter(Boolean)
    let cur_l1 = ''
    let cur_deep = 0
    const lines2 = lines.map((l) => {
        let re = l
        if (l.match(/:/)) {
            // console.log(cur_deep, '  ', l)
            if (cur_deep === 1) {
                cur_l1 = l.replace(/:.*$/, '').trim().replace(/['"]/g, '')
                // console.log('curl1', cur_l1)
            } else if (cur_deep === 2) {
                const k2 = l.replace(/:.*$/, '').trim().replace(/['"]/g, '')
                // console.log('k2', `/${cur_l1}/${k2}`)
                re = l.replace(/'',/, `'/${cur_l1}/${k2}'`)
            }
        }
        if (l.match(/\{/)) {
            cur_deep++
        }
        if (l.match(/\}/)) {
            cur_deep--
        }
        return re
    })
    const txt_re =
        lines2.join('\n').replace(
            'export const raw =',
            `declare namespace ROUT {
interface target `,
        ) + '\n}'
    // console.log(lines2.join('\n'))
    return txt_re
}

fs.writeFileSync(path.join(__dirname, 'computed', 'target-type.d.ts'), txt_type)
// --- raw 现在没用
// const txt_raw = def_txt.replace('raw', 'raw: ROUT.target')
// fs.writeFileSync(path.join(__dirname, 'computed', 'raw.ts'), txt_raw)

// --- l1枚举
const l1_li = Object.keys(raw)
const txt_l1 = make_l1_txt(l1_li)
// console.log(txt_l1.toString())
fs.writeFileSync(path.join(__dirname, 'computed', 'l1.ts'), txt_l1)

function make_l1_txt(li) {
    const temp = `
export const l1 = {
    ${li.map((k) => `'${k}': '${k}',`).join('\n    ')}
} as const
    `
    return temp
}

// --- targe
const enum_li = []
const obj2 = JSON.parse(JSON.stringify(raw))
for (const k1 in obj2) {
    const node1 = obj2[k1]
    for (const k2 in node1) {
        node1[k2] = `/${k1}/${k2}`
        enum_li.push(node1[k2])
        // const node2 = node1[k2]
        // console.log('--', k1, k2, node2)
    }
}
const txt_target = 'export const target: ROUT.target = ' + JSON.stringify(obj2)
fs.writeFileSync(path.join(__dirname, 'computed', 'target.ts'), txt_target)

const txt_enum = `
declare namespace ROUT {
    type target_enum = 
| ${enum_li.map((s) => `'${s}'`).join(' | ')}
 
}
`
fs.writeFileSync(path.join(__dirname, 'computed', 'target-enum.d.ts'), txt_enum)
