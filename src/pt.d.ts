declare namespace PT {
    type key_key<O extends Record<string, any>> = {
        [k in keyof O]: K
    }
    /** 主进程返回结果 */
    interface ipcRe<T extends any = null> {
        success: boolean
        msg: string
        data: T
    }
}
