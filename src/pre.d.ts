declare interface Window {
    electronc: PT.elecipc
}

declare namespace PT {
    /** 向页面进程注入的对象 */
    interface elecipc {
        pick_dir(): PT.ipcRe<string>
        pick_file(extensions: string[]): PT.ipcRe<string>
        pick_files(): PT.ipcRe<string[]>
        read_file(src: string): PT.ipcRe<string>
        /** 加载app配置 */
        load_app_option(): PT.ipcRe<App.item>
        load_books(srcs: string[]): PT.ipcRe<Book.item[]>
        load_book(src: string): PT.ipcRe<Book.item>
        save_book(book: Book.item): PT.ipcRe<Book.item>
        publish(hand: (arg: any) => void): void
    }
}
