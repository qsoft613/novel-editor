// See the Electron documentation for details on how to use preload scripts:

// https://www.electronjs.org/docs/latest/tutorial/process-model#preload-scripts
const { contextBridge, ipcRenderer } = require('electron')

const inst: PT.elecipc = {
    pick_dir() {
        return sendSync('pick_dir')
    },
    pick_file(extensions) {
        return sendSync('pick_file', extensions)
    },
    pick_files() {
        return sendSync('pick_files')
    },
    read_file(src: string) {
        return sendSync('read_file', src)
    },
    load_app_option() {
        return sendSync('load_app_option')
    },
    load_books(srcs) {
        return sendSync('load_books', srcs)
    },
    load_book(src) {
        return sendSync('load_book', src)
    },
    save_book(book) {
        return sendSync('save_book', book)
    },
    publish(hand) {
        ipcRenderer.on('publish', (e, arg) => {
            if (typeof arg !== 'object') {
                return
            }
            hand(arg)
        })
    },
}

contextBridge.exposeInMainWorld('electronc', inst)

function sendSync(key: keyof PT.elecipc, ...args: any[]) {
    return ipcRenderer.sendSync(key, ...args)
}
