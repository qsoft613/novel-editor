const cp = require('child_process')
const fs = require('fs')
const path = require('path')
const os = require('os')

husk()
// envs()

function husk() {
    cp.execSync(`husky install`)

    setTimeout(() => {
        const src = path.join(__dirname, '../.husky/pre-commit')
        if (!fs.existsSync(src)) {
            cp.execSync(` npx husky add .husky/pre-commit "npx lint-staged" `)
        }
    }, 1000)
}

function envs() {
    const ip_address = ga()
    const li = [
        path.join(__dirname, '../packages/admin-operator/.env.local'),
        path.join(__dirname, '../packages/admin-merchant/.env.local'),
        path.join(__dirname, '../packages/mobile/.env.local'),
    ]
    li.forEach((src) => {
        if (!fs.existsSync(src)) {
            fs.writeFileSync(
                src,
                `
# VITE_api=/apilocal/
            `.trim(),
            )
        }
        // ---
        const src2 = path.join(src, '../.host.local')
        if (!fs.existsSync(src2)) {
            fs.writeFileSync(
                src2,
                `
http://${ip_address}:20221/            
            `.trim(),
            )
        }
    })

    function ga() {
        let address = ''
        const ins = os.networkInterfaces()
        Object.values(ins).forEach((li) => {
            li?.forEach((it) => {
                if (it.family === 'IPv4' && !it.internal && it.address !== '127.0.0.1') {
                    // console.log(it)
                    if (!address) {
                        address = it.address
                    }
                }
            })
        })
        return address
    }
}
