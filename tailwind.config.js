const plugin = require('tailwindcss/plugin')

/** @type {import('tailwindcss').Config} */
const options = {
    prefix: '',
    content: ['./src/**/*.tsx', './src/**/*.css'],
    theme: {
        extend: {
            colors: {
                qy: {
                    50: 'var(--clr-50)',
                    100: 'var(--clr-100)',
                    200: 'var(--clr-200)',
                    300: 'var(--clr-300)',
                    400: 'var(--clr-400)',
                    500: 'var(--clr-500)',
                    600: 'var(--clr-600)',
                    700: 'var(--clr-700)',
                    800: 'var(--clr-800)',
                    900: 'var(--clr-900)',
                    front: 'var(--clr-front)',
                    danger: 'var(--clr-danger)',
                },
            },
        },
    },
    plugins: [
        plugin(function ({ addComponents, theme, addUtilities }) {
            addUtilities({
                '.text-0': {
                    fontSize: '0',
                },
            })
            addComponents({
                '.qy-click-able': {},
                '.qy-click-able2': {},
                '.qy-click-notable': {},
            })
        }),
    ],
    corePlugins: {
        preflight: false,
    },
}

module.exports = options
